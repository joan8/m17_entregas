﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Casa : MonoBehaviour
{
    private Vector3 worldPoint;
    private Vector2 worldPoint2d;
    private CursorMode cursorMode = CursorMode.Auto;
    private Vector2 hotSpot = Vector2.zero;
    private Texture2D sword;
    private Texture2D clock;
    private Texture2D StaaaaFinga;
    private Texture2D Goto;
    private Control control;
    public Sprite[] sp;
    private bool start;
    private int fase;
    public Vector2 final { set; get; }
    public Vector2 init { set; get; }
    public bool click { set; get; }
    public bool OnDrag { set; get; }
    public bool coor { set; get; }
    public int madera { set; get; }
    public int stone { set; get; }
    // Start is called before the first frame update
    void Start()
    {
        this.tag = "time";
        this.name = "Casa";
        control = GameObject.Find("GameControl").GetComponent<Control>();
        this.GetComponent<SpriteRenderer>().sprite = sp[0];
        this.start = false;
        //Cursores
        sword = control.getSword();
        clock = control.getClock();
        StaaaaFinga = control.getStaaaaFinga();
        Goto = control.getGoto();
        //Hasta aqui
        this.coor = false;
    }

    // Update is called once per frame
    void Update()
    {
        //Transformamos el mundo a 2d para coger las coordenadas
        this.worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.worldPoint2d = new Vector2(this.worldPoint.x, this.worldPoint.y);
        if (this.click)
        {
            //Si es click, le assignamos al vector 2d, esa posicion del mapa, que es donde se construira el edicicio
            this.final = this.worldPoint2d;
            this.click = false;
        }
        //Hasta que no hagamos el drag a la posicion que lo queremos construir, no empezara a construirse
        if (!this.OnDrag && this.coor)
        {
            if (!start)
            {
                StartCoroutine(Construct());
                this.start = true;
            }
           
        }
    }
    private void OnMouseUp()
    {
        this.transform.position = this.final;
        this.OnDrag = false;
        coor = true;
    }
  
    private void OnMouseDrag()
    {
        if (!coor)
        {
            Cursor.SetCursor(Goto, hotSpot, cursorMode);
            this.click = true;
            this.OnDrag = true;
        }

    }
    void OnMouseEnter()
    {
        if (gameObject.tag == "time")
        {
            Cursor.SetCursor(clock, hotSpot, cursorMode);
        }
    }
    void OnMouseExit()
    {
        Cursor.SetCursor(sword, hotSpot, cursorMode);
    }
  
    IEnumerator Construct()
    {
        //Le ponemos el tag time, para que sea una estructura sin uso, una vez acabada, se le da el tag adequado, para que se puede interactuar con el, en este caso, la casa, su unico proposito es subir el limite de poblacion
        this.gameObject.tag = "time";
        yield return new WaitForSeconds(4f);
        this.GetComponent<SpriteRenderer>().sprite = sp[1];
        yield return new WaitForSeconds(4f);
        this.GetComponent<SpriteRenderer>().sprite = sp[2];
        yield return new WaitForSeconds(4f);
        this.GetComponent<SpriteRenderer>().sprite = sp[3];
        this.gameObject.tag = "casa";

    }
}
