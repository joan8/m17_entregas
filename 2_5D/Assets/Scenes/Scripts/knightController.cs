﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class knightController : MonoBehaviour
{

  
    private Vector3 worldPoint;
    private Vector2 worldPoint2d;
    public Animator animator;
    public bool click { set; get; }
    public bool OnDrag { set; get; }
    private bool dreta;
    private bool esquerra;
    private bool abaix;
    private bool adalt;
    public bool finalMx { set; get; }
    public bool finalMy { set; get; }
    public Vector2 final { set; get; }
    public Vector2 init { set; get; }


    private float vida { set; get; }


    private int ataque { set; get; }

    public int carne { set; get; }


    //Area de accion de knight
    public GameObject ataqueKnight;





    void Start()
    {
        this.gameObject.name = "knight";
        vida = 20;
        ataque = 5;
        carne = 10;
        this.click = false;
        this.init = this.transform.position;
        this.final = this.transform.position;
        this.finalMx = false;
        this.finalMy = false;
    }

    // Update is called once per frame
    void Update()
    {
        this.worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.worldPoint2d = new Vector2(this.worldPoint.x, this.worldPoint.y);
        if (this.click)
        {
            this.final = this.worldPoint2d;
            this.click = false;
        }
        Move();
        

    }

    public void Move()
    {

        if (!this.OnDrag)
        {

            if (this.init.x > this.final.x - 0.05f && this.init.x > this.final.x + 0.05f && !this.finalMx)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, this.GetComponent<Rigidbody2D>().velocity.y);
                this.esquerra = true;
                if (this.transform.position.x <= this.final.x - 0.05f && this.transform.position.x <= this.final.x + 0.05f && !this.finalMx)
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    this.init = this.transform.position;
                    this.finalMx = true;
                    this.esquerra = false;
                }
            }
            else if (this.init.x < this.final.x - 0.05f && this.init.x < this.final.x + 0.05f && !this.finalMx)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(5, this.GetComponent<Rigidbody2D>().velocity.y);
                this.dreta = true;
                if (this.transform.position.x >= this.final.x - 0.05f && this.transform.position.x >= this.final.x + 0.05f && !this.finalMx)
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    this.init = this.transform.position;
                    this.finalMx = true;
                    this.dreta = false;
                }
            }
            if (this.init.y > this.final.y - 0.05f && this.init.y > this.final.y + 0.05f && !this.finalMy)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -5);
                this.abaix = true;
                if (this.transform.position.y <= this.final.y - 0.05f && this.transform.position.y <= this.final.y + 0.05f && !this.finalMy)
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    this.init = this.transform.position;
                    this.finalMy = true;
                    this.abaix = false;
                }
            }
            else if (this.init.y < this.final.y - 0.05f && this.init.y < this.final.y + 0.05f && !this.finalMy)
            {
                this.adalt = true;
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 5);
                if (this.transform.position.y >= this.final.y - 0.05f && this.transform.position.y >= this.final.y + 0.05f && !this.finalMy)
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    this.init = this.transform.position;
                    this.finalMy = true;
                    this.adalt = false;
                }
            }

        }
        MoveAnimation();
    }

    public void MoveAnimation()
    {
        if (this.dreta && !this.esquerra)
        {
            if (!this.adalt && !this.abaix)
            {
                this.animator.SetBool("walkRight", true);
                this.animator.SetBool("walkLeft", false);
                this.animator.SetBool("walkUp", false);
                this.animator.SetBool("walkDown", false);
            }
            if (this.adalt)
            {
                this.animator.SetBool("walkRight", true);
                this.animator.SetBool("walkLeft", false);
                this.animator.SetBool("walkUp", true);
                this.animator.SetBool("walkDown", false);
            }
            else if (this.abaix)
            {
                this.animator.SetBool("walkRight", true);
                this.animator.SetBool("walkLeft", false);
                this.animator.SetBool("walkUp", false);
                this.animator.SetBool("walkDown", true);
            }
        }
        if (this.esquerra && !this.dreta)
        {
            if (!this.adalt && !this.abaix)
            {
                this.animator.SetBool("walkRight", false);
                this.animator.SetBool("walkLeft", true);
                this.animator.SetBool("walkUp", false);
                this.animator.SetBool("walkDown", false);
            }
            if (this.adalt)
            {
                this.animator.SetBool("walkRight", false);
                this.animator.SetBool("walkLeft", true);
                this.animator.SetBool("walkUp", true);
                this.animator.SetBool("walkDown", false);
            }
            else if (this.abaix)
            {
                this.animator.SetBool("walkRight", false);
                this.animator.SetBool("walkLeft", true);
                this.animator.SetBool("walkUp", false);
                this.animator.SetBool("walkDown", true);
            }
        }
        if (!this.dreta && !this.esquerra)
        {
            if (this.adalt)
            {
                this.animator.SetBool("walkRight", false);
                this.animator.SetBool("walkLeft", false);
                this.animator.SetBool("walkUp", true);
                this.animator.SetBool("walkDown", false);
            }
            else if (this.abaix)
            {
                this.animator.SetBool("walkRight", false);
                this.animator.SetBool("walkLeft", false);
                this.animator.SetBool("walkUp", false);
                this.animator.SetBool("walkDown", true);
            }

        }
        if (!this.dreta && !this.esquerra && !this.abaix && !this.adalt)
        {
            this.animator.SetBool("walkRight", false);
            this.animator.SetBool("walkLeft", false);
            this.animator.SetBool("walkUp", false);
            this.animator.SetBool("walkDown", false);
        }
    }

    private void OnMouseUp()
    {
        if (this.tag == "knight")
        {
            this.GetComponent<BoxCollider2D>().isTrigger = true;
        }
        this.transform.position = this.init;
        this.OnDrag = false;
    }
    private void OnMouseDrag()
    {
        
        this.GetComponent<BoxCollider2D>().isTrigger = false;
        this.click = true;
        this.OnDrag = true;
        this.finalMx = false;
        this.finalMy = false;
    }
}
