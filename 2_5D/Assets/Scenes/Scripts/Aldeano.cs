﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class Aldeano : MonoBehaviour
{
    /*Clase que controla el aldeano. Se mueve con a*path. Anteriormente lo controlabamos con drags, por lo que el codigo se ha comentado. El aldeano puede conseguir recursos quedandose al lado de la fuente
     en nuestro caso, los arboles y las minas. Esta controlado con corrutinas*/

    private GameObject menu;
    private Home home;
    private Palmera arbolmadera;
    private Stone stoneStone;
    private CursorMode cursorMode = CursorMode.Auto;
    private Vector2 hotSpot = Vector2.zero;
    private Control control;
    private Vector3 worldPoint;
    private Vector2 worldPoint2d;
    private Texture2D sword;
    private Texture2D clock;
    private Texture2D StaaaaFinga;
    private Texture2D Goto;
   // public Animator animator;
    public bool click { set; get; }
    public bool OnDrag { set; get; }
    public bool dreta;
    public bool esquerra;
    public bool abaix;
    public bool adalt;
    public bool finalMx { set; get; }
    public bool finalMy { set; get; }
    public Vector2 final { set; get; }
    public Vector2 init { set; get; }
    private bool menuA;
    public bool arbol { set; get; }
    public bool piedra { set; get; }
    public bool work { set; get; }
    public bool setR { set; get; }
    public bool col { set; get; }

    public int madera { set; get; }
    public int stone { set; get; }
    public int meat { set; get; }
    private bool getciudad;
    public bool coli { set; get; }
    public bool clicked { set; get; }

    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.name = "Aldeano";
        control = GameObject.Find("GameControl").GetComponent<Control>();
        menu = GameObject.Find("Menu");
        this.click = false;
        this.init = this.transform.position;
        this.final = this.transform.position;
        this.finalMx = false;
        this.finalMy = false;
        //Los iconos del mouse
        sword = control.getSword();
        clock = control.getClock();
        StaaaaFinga = control.getStaaaaFinga();
        Goto = control.getGoto();
        //Hasta aqui
        setR = true;
        getciudad = false;
        //Recursos
        meat = 0;
        madera = 0;
        stone = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!getciudad && GameObject.Find("TownPlace"))
        {
            //Assignamos la home cuando no es null
            home = GameObject.Find("TownPlace").GetComponent<Home>();
            getciudad = true;
        }
      /*  this.worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.worldPoint2d = new Vector2(this.worldPoint.x, this.worldPoint.y);*/
        if (this.click)
        {
            this.final = this.worldPoint2d;
            this.click = false;
        }
      //  Move();
        CortarArbol();
        CortarStone();
    }
    public void Move()
    {
      
        /*if (!this.OnDrag)
        {
           
            if (this.init.x > this.final.x - 0.05f && this.init.x > this.final.x + 0.05f && !this.finalMx)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, this.GetComponent<Rigidbody2D>().velocity.y);
                this.esquerra = true;
                if ((this.transform.position.x <= this.final.x - 0.05f && this.transform.position.x <= this.final.x + 0.05f && !this.finalMx) || col)
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    this.init = this.transform.position;
                    this.finalMx = true;
                    this.esquerra = false;
                    col = false;
                }
            }
            else if (this.init.x < this.final.x-0.05f && this.init.x < this.final.x+0.05f && !this.finalMx)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(5, this.GetComponent<Rigidbody2D>().velocity.y);
                this.dreta = true;
                if ((this.transform.position.x >= this.final.x - 0.05f && this.transform.position.x >= this.final.x + 0.05f && !this.finalMx) || col)
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    this.init = this.transform.position;
                    this.finalMx = true;
                    this.dreta = false;
                    col = false;
                }
            }
            if (this.init.y > this.final.y - 0.05f && this.init.y > this.final.y + 0.05f && !this.finalMy)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -5);
                this.abaix = true;
                if ((this.transform.position.y <= this.final.y - 0.05f && this.transform.position.y <= this.final.y + 0.05f && !this.finalMy) || col)
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    this.init = this.transform.position;
                    this.finalMy = true;
                    this.abaix = false;
                    col = false;
                }
            }
            else if (this.init.y < this.final.y - 0.05f && this.init.y < this.final.y + 0.05f && !this.finalMy)
            {
                this.adalt = true;
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 5);
                if ((this.transform.position.y >= this.final.y - 0.05f && this.transform.position.y >= this.final.y + 0.05f && !this.finalMy) || col)
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    this.init = this.transform.position;
                    this.finalMy = true;
                    this.adalt = false;
                    col = false;
                }
            }

        }
        MoveAnimation();*/
    }
    /*public void MoveAnimation()
    {
        if (this.dreta && !this.esquerra)
        {
            if (!this.adalt && !this.abaix)
            {
                this.animator.SetBool("Right", true);
                this.animator.SetBool("Left", false);
                this.animator.SetBool("Up", false);
                this.animator.SetBool("Down", false);
            }
            if (this.adalt)
            {
                this.animator.SetBool("Right", true);
                this.animator.SetBool("Left", false);
                this.animator.SetBool("Up", true);
                this.animator.SetBool("Down", false);
            }else if (this.abaix)
            {
                this.animator.SetBool("Right", true);
                this.animator.SetBool("Left", false);
                this.animator.SetBool("Up", false);
                this.animator.SetBool("Down", true);
            }
        }
        if(this.esquerra && !this.dreta)
        {
            if (!this.adalt && !this.abaix)
            {
                this.animator.SetBool("Right", false);
                this.animator.SetBool("Left", true);
                this.animator.SetBool("Up", false);
                this.animator.SetBool("Down", false);
            }
            if (this.adalt)
            {
                this.animator.SetBool("Right", false);
                this.animator.SetBool("Left", true);
                this.animator.SetBool("Up", true);
                this.animator.SetBool("Down", false);
            }
            else if (this.abaix)
            {
                this.animator.SetBool("Right", false);
                this.animator.SetBool("Left", true);
                this.animator.SetBool("Up", false);
                this.animator.SetBool("Down", true);
            }
        }
        if (!this.dreta && !this.esquerra)
        {
            if (this.adalt)
            {
                this.animator.SetBool("Right", false);
                this.animator.SetBool("Left", false);
                this.animator.SetBool("Up", true);
                this.animator.SetBool("Down", false);
            }
            else if (this.abaix)
            {
                this.animator.SetBool("Right", false);
                this.animator.SetBool("Left", false);
                this.animator.SetBool("Up", false);
                this.animator.SetBool("Down", true);
            }

        }
        if (!this.dreta && !this.esquerra && !this.abaix && !this.adalt)
        {
            stopMotion();
        }
    }*/
    
    private void OnMouseUp()
    {
        if (this.tag == "Aldeano")
        {
            //this.GetComponent<BoxCollider2D>().isTrigger = true;
        }
        //this.transform.position = this.init;
        this.OnDrag = false;
    }
    private void OnMouseDrag()
    {
        Cursor.SetCursor(Goto, hotSpot, cursorMode);
       // this.GetComponent<BoxCollider2D>().isTrigger = false;
        this.click = true;
        this.OnDrag = true;
        this.finalMx = false;
        this.finalMy = false;
    }
    void OnMouseDown()
    {
        //Activamos y descativamos los menus
        if (menuA)
        {
            menu.transform.GetChild(0).gameObject.SetActive(false);
            menu.transform.GetChild(1).gameObject.SetActive(false);
            menuA = false;
        }
        else
        {
            menu.transform.GetChild(0).gameObject.SetActive(true);
            menuA = true;
        }
        
    }
    void OnMouseEnter()
    {
      
        if (gameObject.tag == "Aldeano")
        {
            Cursor.SetCursor(StaaaaFinga, hotSpot, cursorMode);
        }
        else
        {
            Cursor.SetCursor(sword, hotSpot, cursorMode);
        }
    }

    void OnMouseExit()
    {
        Cursor.SetCursor(sword, hotSpot, cursorMode);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "tree")
        {
            col = true;
            arbol = true;
            arbolmadera = collision.gameObject.GetComponent<Palmera>();
           // stopMotion();
        }
        if (collision.tag == "stone")
        {
            col = true;
            piedra = true;
            stoneStone = collision.gameObject.GetComponent<Stone>();
          //  stopMotion();
        }
     
        if (collision.tag == "home" && this.setR)
        {
            col = true;
           // StartCoroutine(Resources());
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "tree")
        {
            arbol = false;
        }if ( collision.tag == "stone")
        {
            piedra = false;
        }
        if (collision.tag == "home")
        {
            this.setR = true;
        }
    }
    
    IEnumerator cutArbol()
    {
        yield return new WaitForSeconds(0.5f);
        //Cada 0.5 adquirimos 5 de madera y le restamos 5 a ese arbol
        this.madera += 5;
        arbolmadera.madera -= 5;
        work = false;
    }
    /*IEnumerator Resources()
    {
        yield return new WaitForSeconds(0.001f);
        this.madera = 0;
        this.stone = 0;
        this.setR = false;
        stopMotion();
    }*/
    public void CortarArbol()
    {
        if (!work && arbol)
        {
            work = true;
            StartCoroutine(cutArbol());
        }
    }
    IEnumerator cutStone()
    {
        //Lo mismo que la madera
        yield return new WaitForSeconds(0.5f);
        this.stone += 5;
        stoneStone.stone -= 5;
        work = false;
    }
    public void CortarStone()
    {
        if (!work && piedra)
        {
            work = true;
            StartCoroutine(cutStone());
        }
    }
   /* public void stopMotion()
    {
        adalt = false;
        esquerra = false;
        dreta = false;
        abaix = false;
      /*  this.animator.SetBool("Right", false);
        this.animator.SetBool("Left", false);
        this.animator.SetBool("Up", false);
        this.animator.SetBool("Down", false);
    }*/
    /* private void OnCollisionEnter2D(Collision2D collision)
     {
         adalt = false;
         esquerra = false;
         dreta = false;
         abaix = false;
         stopMotion();
         this.transform.position = this.transform.position;
         this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
         col = true;
     }*/
  /*  private void OnCollisionExit2D(Collision2D collision)
    {
        col = false;     
    }*/
}
