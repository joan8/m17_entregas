﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameController : MonoBehaviour
{

    //Canvas
    public Canvas menuInferior;

    //Para los sonidos
    public AudioSource audioSource;
    public AudioClip aldeanoClick;

    //Recursos
    int madera = 0;
    int piedra = 0;
    int carne = 10;

    //Para instanciar tropas o aldeanos o lo que pollas quieras instanciar
    public GameObject aldeanoPrefab;
    public GameObject knightPrefab;
    public GameObject baluarte;
   
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);            

            //Si donde clicamos no es nulo
            if (hit.collider != null)
            {
                //Si clicamos en el baluarte
                if (hit.collider.gameObject.tag=="baluarte")
                {
                    menuInferior.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
                } else if(hit.collider.gameObject.tag == "aldeano")
                {
                    //audioSource.PlayOneShot(aldeanoClick);
                    menuInferior.transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
                } else
                {
                    menuInferior.transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
                }                
            }

        }

        //Actualizar recursos en el canvas
        //Carne
        menuInferior.transform.GetChild(1).GetChild(0).GetComponent<UnityEngine.UI.Text>().text = carne.ToString();



    }

    public void crearHumano()
    {

        GameObject a = Instantiate(aldeanoPrefab) as GameObject;
        a.transform.position = new Vector3(baluarte.transform.position.x+1, baluarte.transform.position.y - 0.5f, baluarte.transform.position.z);

    }

    public void crearKnight()
    {

        GameObject b = Instantiate(knightPrefab) as GameObject;
        b.transform.position = new Vector3(baluarte.transform.position.x + 1, baluarte.transform.position.y - 0.5f, baluarte.transform.position.z);

    }
}
