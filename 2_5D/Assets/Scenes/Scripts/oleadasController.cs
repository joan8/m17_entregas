﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class oleadasController : MonoBehaviour
{

    public GameObject osoPrefab;
    private GameObject Osos;
    private GameObject cg;
    private CanvasMenu menu;
    bool creado = false;
    public float respawnTime = 60.0f;
    public bool coroutinDone = true;
    public int contador = 0;
    private float paradaEntreFase = 60.0f;
    int fase;
    public int oleada { set; get; }

    //gameObject para pillar la posicion de spawn
    public GameObject spawnerPosition;
    private bool cambioDeFase = false;

    // Start is called before the first frame update
    void Start()
    {
        menu = GameObject.Find("Menu").GetComponent<CanvasMenu>();
        fase = 4;
        Osos = GameObject.Find("Osos");
        respawnTime = 45.0f;
        paradaEntreFase = 60.0f;
        cambioDeFase = false;
        oleada = 1;
    }

    // Update is called once per frame
    void Update()
    {

        
        //Cuando el jugador construye el cuartel, el generador de oleadas empieza a generar oleadas. Tambien se puede añadir la condicion que pase un minimo de tiempo pero estoy hasta los cojones y hay que acabar esto.
        if (GameObject.FindGameObjectWithTag("home")!=null && !creado)
        {
            cg = GameObject.FindGameObjectWithTag("home");
            creado = true;
            coroutinDone = false;
        }

        if (creado)
        {
            if (contador <= fase)
            {
                StartCoroutine(oleadasCoroutine());
            }
           /* else
            {
                StartCoroutine(paradaEntreFaseYfase());
            }*/
        }
       
        
    }
    IEnumerator oleadasCoroutine()
    {
        if (contador < fase)
        {
            yield return new WaitForSeconds(respawnTime);
            GameObject a = Instantiate(osoPrefab) as GameObject;
            a.transform.position = new Vector2(spawnerPosition.transform.position.x, spawnerPosition.transform.position.y);
            a.transform.parent = Osos.transform;
            coroutinDone = false;
            if (respawnTime > 20.0f && contador == 1)
            {
                respawnTime -= 5.0f;
            }
            contador++;
        }
        else if (contador == fase)
        {
            yield return new WaitForSeconds(paradaEntreFase);
            fase++;
            oleada++;
            menu.oleada = this.oleada;
            print(oleada);
            if (paradaEntreFase > 30.0f)
            {
                paradaEntreFase -= 5f;
            }
            coroutinDone = true;
            contador = 0;
        }
        StopAllCoroutines();
    }

    /* IEnumerator paradaEntreFaseYfase()
     {
         yield return new WaitForSeconds(paradaEntreFase);
         fase++;
         oleada++;
         if (paradaEntreFase > 30.0f)
         {
             paradaEntreFase -= 5f;
         }
         coroutinDone = true;
         contador = 0;
         StopCoroutine(paradaEntreFaseYfase());

     }*/

}
