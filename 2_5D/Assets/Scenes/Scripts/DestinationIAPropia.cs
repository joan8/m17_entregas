﻿using System.Collections;
using UnityEngine;

namespace Pathfinding
{
    /// <summary>
    /// Sets the destination of an AI to the position of a specified object.
    /// This component should be attached to a GameObject together with a movement script such as AIPath, RichAI or AILerp.
    /// This component will then make the AI move towards the <see cref="target"/> set on this component.
    ///
    /// See: <see cref="Pathfinding.IAstarAI.destination"/>
    ///
    /// [Open online documentation to see images]
    /// </summary>
    [UniqueComponent(tag = "ai.destination")]
    [HelpURL("http://arongranberg.com/astar/docs/class_pathfinding_1_1_a_i_destination_setter.php")]
    public class DestinationIAPropia : VersionedMonoBehaviour
    {
        private AstarPath A;
        private Transform target;        
        IAstarAI ai;
        private bool clicked;
        // Start is called before the first frame update
        void Start()
        {
            //Hemos variado un poco el script original, con la diferencia que ahora, le haces doble click a donde quieres ir, despues, le indicas el aldeano que quieres que vaya hasta la posicion deignada.
            target = GameObject.Find("Target").GetComponent<Transform>();            
            A = GameObject.Find("A*").GetComponent<AstarPath>();
            clicked = false;
        }
        void OnEnable()
        {
            ai = GetComponent<IAstarAI>();
            // Update the destination right before searching for a path as well.
            // This is enough in theory, but this script will also update the destination every
            // frame as the destination is used for debugging and may be used for other things by other
            // scripts as well. So it makes sense that it is up to date every frame.
            if (ai != null) ai.onSearchPath += Update;
        }

        void OnDisable()
        {
            if (ai != null) ai.onSearchPath -= Update;
        }

        /// <summary>Updates the AI's destination every frame</summary>
        void Update()
        {
            if (target != null && ai != null && clicked)
            {
                ai.destination = target.position;
               
                clicked = false;
            }
            if (ai.destination == target.position && target != null && ai != null)
            {
                A.UpdateGraphs(this.GetComponent<CapsuleCollider2D>().bounds);
            }
        }
        private void OnMouseDown()
        {
            clicked = true;
        }
    }
}