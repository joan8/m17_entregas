﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlAnimation : MonoBehaviour
{
    private Transform target;
    public bool dreta;
    public bool esquerra;
    public bool abaix;
    public bool adalt;
    public Animator animator;
    private Vector2 ant;
    

    // Start is called before the first frame update
    void Start()
    {
        //Assignamos el vector actual a la variable para usarlo luego
        this.ant = this.transform.position;
       // target = GameObject.Find("Target").GetComponent<Transform>();
        adalt = false;
        esquerra = false;
        dreta = false;
        abaix = false;
    }

    // Update is called once per frame
    void Update()
    {
       
        Move();
        StartCoroutine(moveC());
        
    }
    public void Move()
    {
       //Si la posicion de x guardada anteriormente es su actual en x va hacia la izquierda
        if (this.ant.x > this.transform.position.x)
        {
            this.esquerra = true;
            //Dentro del margen de 0.4 de su posicion, le indicamos que ya no se mueve hacia la izquierda para que cambie su animacion.
            if (this.ant.x >= this.transform.position.x - 0.2f && this.ant.x <= this.transform.position.x + 0.2f)
            {
                this.esquerra = false;
            }
        }
        else if (this.ant.x < this.transform.position.x)
        {
            this.dreta = true;
            if (this.ant.x >= this.transform.position.x - 0.2f && this.ant.x <= this.transform.position.x + 0.2f)
            {
                this.dreta = false;
            }
        }
        if (this.ant.y > this.transform.position.y)
        {

            this.abaix = true;
            if (this.ant.y >= this.transform.position.y - 0.2f && this.ant.y <= this.transform.position.y + 0.2f)
            {
                this.abaix = false;
            }
        }
        else if (this.ant.y < this.transform.position.y)
        {
            this.adalt = true;
            if (this.ant.y >= this.transform.position.y - 0.2f && this.ant.y <= this.transform.position.y + 0.2f)
            {
                this.adalt = false;
            }
        } /*
        if (this.transform.position.x > this.target.position.x)
        {

            this.esquerra = true;
            if (this.transform.position.x >= this.target.position.x - 0.2f && this.transform.position.x <= this.target.position.x + 0.2f)
            {
                this.esquerra = false;
            }
        }
        else if (this.transform.position.x < this.target.position.x )
        {
            this.dreta = true;
            if (this.transform.position.x >= this.target.position.x - 0.2f && this.transform.position.x <= this.target.position.x + 0.2f)
            {
                this.dreta = false;
            }
        }
        if (this.transform.position.y > this.target.position.y)
        {

            this.abaix = true;
            if (this.transform.position.y >= this.target.position.y - 0.2f && this.transform.position.y <= this.target.position.y + 0.2f)
            {
                this.abaix = false;
            }
        }
        else if (this.transform.position.x < this.target.position.x )
        {
            this.adalt = true;
            if (this.transform.position.y >= this.target.position.y - 0.2f && this.transform.position.y <= this.target.position.y + 0.2f)
            {
                this.adalt = false;
            }
        }

       */
        MoveAnimation();
    }
    public void MoveAnimation()
    {
        if (this.dreta && !this.esquerra)
        {
            if (!this.adalt && !this.abaix)
            {
                this.animator.SetBool("Right", true);
                this.animator.SetBool("Left", false);
                this.animator.SetBool("Up", false);
                this.animator.SetBool("Down", false);
            }
            if (this.adalt)
            {
                this.animator.SetBool("Right", true);
                this.animator.SetBool("Left", false);
                this.animator.SetBool("Up", true);
                this.animator.SetBool("Down", false);
            }
            else if (this.abaix)
            {
                this.animator.SetBool("Right", true);
                this.animator.SetBool("Left", false);
                this.animator.SetBool("Up", false);
                this.animator.SetBool("Down", true);
            }
        }
        if (this.esquerra && !this.dreta)
        {
            if (!this.adalt && !this.abaix)
            {
                this.animator.SetBool("Right", false);
                this.animator.SetBool("Left", true);
                this.animator.SetBool("Up", false);
                this.animator.SetBool("Down", false);
            }
            if (this.adalt)
            {
                this.animator.SetBool("Right", false);
                this.animator.SetBool("Left", true);
                this.animator.SetBool("Up", true);
                this.animator.SetBool("Down", false);
            }
            else if (this.abaix)
            {
                this.animator.SetBool("Right", false);
                this.animator.SetBool("Left", true);
                this.animator.SetBool("Up", false);
                this.animator.SetBool("Down", true);
            }
        }
        if (!this.dreta && !this.esquerra)
        {
            if (this.adalt)
            {
                this.animator.SetBool("Right", false);
                this.animator.SetBool("Left", false);
                this.animator.SetBool("Up", true);
                this.animator.SetBool("Down", false);
            }
            else if (this.abaix)
            {
                this.animator.SetBool("Right", false);
                this.animator.SetBool("Left", false);
                this.animator.SetBool("Up", false);
                this.animator.SetBool("Down", true);
            }

        }
        if (!this.dreta && !this.esquerra && !this.abaix && !this.adalt)
        {
            stopMotion();
        }
    }
    public void stopMotion()
    {
        adalt = false;
        esquerra = false;
        dreta = false;
        abaix = false;
      
        this.animator.SetBool("Right", false);
        this.animator.SetBool("Left", false);
        this.animator.SetBool("Up", false);
        this.animator.SetBool("Down", false);
    }
    IEnumerator moveC()
    {
        //Cada 0.5f actualiza su posicion anterior, por si cambia de direcion durante el movimiento
        yield return new WaitForSeconds(0.5f);
        this.ant = this.transform.position;
        StopAllCoroutines();
    }
}
