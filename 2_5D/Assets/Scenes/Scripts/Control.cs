﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;


public class Control : MonoBehaviour
{
    public Texture2D sword;
    public Texture2D clock;
    public Texture2D StaaaaFinga;
    public Texture2D Goto;

    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;
    public int comidaN { get; set; }
    public int maderaN { get; set; }
    public int piedraN { get; set; }
    public bool alclick { get; set; }
    public bool alfinal { get; set; }

    public delegate void madera(int madera);
    public event madera eventMad;

    // Start is called before the first frame update
    void Start()
    {
        //Clase donde assignamos los diferentes tipos de cursores para pasarlos a los edificios.
        Cursor.SetCursor(sword, hotSpot, cursorMode);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseDown()
    {
        
    }
    public Texture2D getSword()
    {
        return sword;
    }
    public Texture2D getClock()
    {
        return clock;
    }
    public Texture2D getStaaaaFinga()
    {
        return StaaaaFinga;
    }
    public Texture2D getGoto()
    {
        return Goto;
    }
}
