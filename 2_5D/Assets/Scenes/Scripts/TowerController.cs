﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour
{
    public Sprite[] fases;
    public int damage, hp, range;
    public int fase;
    public float atVel;
    //private GameObject area;
    public bool clicked;
    private GameObject camara;
  
    void Start()
    {
        fase = 0;
        this.gameObject.GetComponent<SpriteRenderer>().sprite = fases[0];
        damage = 5;
        hp = 200;
        atVel = 1f;  //Dispara cada 2 segundos, aplicar corrutina
        range = 15; // habrá que modificar el tamaño del trigger
        //area = this.transform.parent.GetChild(1).gameObject;
        this.transform.GetChild(0).localScale = new Vector2(range, range);
        clicked = false;
        camara = GameObject.Find("Main Camera");
    }

    void Update()
    {
        switch (fase)
        {
            case 1:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = fases[fase];
                damage = 10;
                hp = 400;
                atVel = 0.75f;
                range = 20;
                break;
            case 2:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = fases[fase];
                damage = 15;
                hp = 700;
                atVel = 0.5f;
                range = 25;
                break;
            case 3:
                this.gameObject.GetComponent<SpriteRenderer>().sprite = fases[fase];
                damage = 20;
                hp = 1000;
                atVel = 0.25f;
                range = 30;
                break;
        }
        this.transform.GetChild(0).transform.localScale = new Vector2(range, range);

    }
    private void OnMouseDown()
    {
        camara.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, camara.transform.position.z);
        if (!clicked)
        {
            this.gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
            this.gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = true;
            clicked = true;
        }
        else
        {
            this.gameObject.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
            this.gameObject.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
            clicked = false;
        } 
    }
}
