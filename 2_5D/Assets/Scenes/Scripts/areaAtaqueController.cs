﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class areaAtaqueController : MonoBehaviour
{
    /*Clase que detecta cuando un knight entra en la zona de ataque del oso enemigo y esta, avisa al padre para que se detenga y empiece el ataque*/

    public GameObject padre;

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag=="ataqueKnight"){
            padre.GetComponent<werebearBehaviour>().pillado=true;
            padre.GetComponent<werebearBehaviour>().atacantes++;
        }    
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "ataqueKnight")
        {
            padre.GetComponent<werebearBehaviour>().atacantes--;
        }
            
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
