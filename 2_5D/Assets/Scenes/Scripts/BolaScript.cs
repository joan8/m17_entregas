﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaScript : MonoBehaviour
{
    private int attack;
    public GameObject objectivo { set; get; }
    public delegate void killed(GameObject killed);
    public event killed killedEvent;

    void Start()
    {
        
    }


    void Update()
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position, objectivo.transform.position, 0.25f);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "werebear")
        {
            killedEvent(collision.gameObject);
        }
        Destroy(this.gameObject);

    }
}
