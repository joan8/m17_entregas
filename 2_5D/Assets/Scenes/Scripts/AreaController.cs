﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AreaController : MonoBehaviour
{
    public Queue<GameObject> colaTargets = new Queue<GameObject>();
    public GameObject tower;
    public GameObject bola;
    public bool disparando = false;
    public delegate void areaKilled(GameObject killed);
    public event areaKilled killedAreaEvent;
    GameObject objetivo;

    void Start()
    {
        tower = this.transform.parent.gameObject;
    }

    void Update()
    {


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "werebear")
        {
            colaTargets.Enqueue(collision.gameObject);
            objetivo = colaTargets.Peek();
            float vida = objetivo.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().fillAmount;
            while (vida > 0)
            {
                GameObject b = Instantiate(bola);
                b.GetComponent<BolaScript>().killedEvent += destruir;
                b.GetComponent<BolaScript>().objectivo = objetivo.gameObject;
                b.transform.position = this.transform.position;
                vida -= tower.GetComponent<TowerController>().damage;
            }
            colaTargets.Dequeue();
        }
    }

    private void destruir(GameObject enemigo)
    {
        killedAreaEvent(enemigo);
    }
}
