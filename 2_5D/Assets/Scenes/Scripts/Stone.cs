﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stone : MonoBehaviour
{
    public int stone { set; get; }
    // Start is called before the first frame update
    void Start()
    {
        //Le decimos la cantidad de recurso que tenemos y le ponemos el colider, para que el A* lo detecte
        this.stone = 150;
        StartCoroutine(coliderS());
    }

    // Update is called once per frame
    void Update()
    {
        if (stone <= 0)
        {
            Destroy(this.gameObject);
        }
    }
    IEnumerator coliderS()
    {
        yield return new WaitForSeconds(0.001f);
        this.gameObject.AddComponent<BoxCollider2D>();
        this.GetComponent<BoxCollider2D>().size = new Vector2(0.9687238f, 0.5858355f);
        this.GetComponent<BoxCollider2D>().offset = new Vector2(0.007148087f, -0.005718708f);
        this.GetComponent<BoxCollider2D>().isTrigger = true;

    }
}