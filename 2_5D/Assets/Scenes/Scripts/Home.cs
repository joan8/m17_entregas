﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Home : MonoBehaviour
{
    private AstarPath A;
    private Aldeano al;
    private knightController kn;
    public int madera { set; get; }
    public int stone{ set; get; }
    public int mead{ set; get; }
    private GameObject menu;
    private Vector3 worldPoint;
    private Vector2 worldPoint2d;
    private CursorMode cursorMode = CursorMode.Auto;
    private Vector2 hotSpot = Vector2.zero;
    private Texture2D sword;
    private Texture2D clock;
    private Texture2D StaaaaFinga;
    private Texture2D Goto;

    private Control control;
    public Sprite[] sp;
    private bool start;
    public int fase { get; set; }

    public delegate void mouseAser();
    public event mouseAser eventClick;

    public Vector2 final { set; get; }
    public Vector2 init { set; get; }
    public bool click { set; get; }
    public bool OnDrag { set; get; }
    public bool coor { set; get; }
    public bool menuA { set; get; }

    public delegate void actuRes(int c,int m, int s);
    public event actuRes eventRes;
    public Image vida;
    


    // Start is called before the first frame update
    void Start()
    {
        //Le ssignamos el A*, para hacer que haga scans.
        A = GameObject.Find("A*").GetComponent<AstarPath>();
        this.tag = "time";
        this.name = "TownPlace";
        control = GameObject.Find("GameControl").GetComponent<Control>();
        menu = GameObject.Find("Menu");
        this.GetComponent<SpriteRenderer>().sprite = sp[0];
        this.start = false;
        //Cursores
        sword = control.getSword();
        clock = control.getClock();
        StaaaaFinga = control.getStaaaaFinga();
        Goto = control.getGoto();
        //Hasta aqui
        this.coor = false;
        this.menuA = false;
        fase = 1;
        this.worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.worldPoint2d = new Vector2(this.worldPoint.x, this.worldPoint.y);
        this.transform.position = new Vector2(worldPoint2d.x = 0, worldPoint2d.y = 0);
        //Aqui rehace el Scan del A*
        InvokeRepeating("RebuildPath",30,30);
        this.madera = 500;
        this.stone = 250;
        this.mead = 100;
    }

    // Update is called once per frame
    void Update()
    {
        //Transformamos el mundo a 2d para coger las coordenadas
        this.worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.worldPoint2d = new Vector2(this.worldPoint.x, this.worldPoint.y);
        if (this.click)
        {
            //Si es click, le assignamos al vector 2d, esa posicion del mapa, que es donde se construira el edicicio
            this.final = this.worldPoint2d;
            this.click = false;
        }
        //Hasta que no hagamos el drag a la posicion que lo queremos construir, no empezara a construirse
        if (!this.OnDrag && this.coor)
        {
            if (!start)
            {
                StartCoroutine(Construct());
                this.start = true;
            }
            //Si le damos al canvas para hacer el upgrade a la home, dependiendo de la fase en la que este tardara mas o menos en realizarla
            if (menu.GetComponent<CanvasMenu>().upHome)
            {
                switch (this.fase)
                {
                    case 1:
                        StartCoroutine(Upgrade1());
                        break;
                    case 2:
                        StartCoroutine(Upgrade2());
                        break;
                    case 3:
                        StartCoroutine(Upgrade3());
                        break;
                }
                menu.GetComponent<CanvasMenu>().upHome = false;
            }
        }

    }
    private void OnMouseUp()
    {
        this.transform.position = this.final;
        this.OnDrag = false;
        coor = true;
    }
    private void OnMouseDown()
    {
        if (this.tag == "home" && !this.menuA)
        {
            menu.transform.GetChild(3).gameObject.SetActive(true);
            this.menuA = true;
        }else
        {
            menu.transform.GetChild(3).gameObject.SetActive(false);
            this.menuA = false;
        }
    }
    private void OnMouseDrag()
    {
        if (!coor)
        {
            Cursor.SetCursor(Goto, hotSpot, cursorMode);
            this.click = true;
            this.OnDrag = true;
        }

    }
    void OnMouseEnter()
    {
        if (gameObject.tag == "time")
        {
            Cursor.SetCursor(clock, hotSpot, cursorMode);
        }
        if (gameObject.tag == "Home")
        {
            Cursor.SetCursor(StaaaaFinga, hotSpot, cursorMode);
        }
    }
    void OnMouseExit()
    {
        Cursor.SetCursor(sword, hotSpot, cursorMode);
    }
   


    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si entra un aldeano, coge todos los recursos que lleva encima, luego los recursos del aldeano los pone a 0 y pasa por un delegado los recursos de la home al canvas para poderlos mostrar.
        if (collision.tag == "aldeano" && collision.GetComponent<Aldeano>().setR)
        {
            this.al = collision.gameObject.GetComponent<Aldeano>();
            this.madera += this.al.madera;
            this.al.madera = 0;
            this.stone += this.al.stone;
            this.al.stone = 0;
            if (this.eventRes != null)
            {
                this.eventRes(this.mead,this.madera,this.stone);
            }
        }
        //Es lo mismo que el aldeano, la diferencia es que el knight solo puede llevar comida y el aldeano, todo menos comida.
        if (collision.tag=="knight")
        {
            this.kn = collision.gameObject.GetComponent<knightController>();
            this.mead += this.kn.carne;
            this.kn.carne = 0;
            if (this.eventRes != null)
            {
                this.eventRes(this.mead, this.madera, this.stone);
            }
        }
        //Si entra un enemigo, este le baja vida, si llega a 0 fin de partida
        if (collision.tag=="werebearOleada")
        {
            vida.gameObject.transform.GetComponent<Image>().fillAmount -= 0.05f;
            if (vida.gameObject.transform.GetComponent<Image>().fillAmount == 0)
            {
                Destroy(this.gameObject);
                SceneManager.LoadScene("Final");
            }
            //Destruye al enemigo
            Destroy(collision.gameObject);
        }

    }

    
    IEnumerator Construct()
    {
        this.gameObject.tag = "time";
        yield return new WaitForSeconds(5f);
        this.GetComponent<SpriteRenderer>().sprite = sp[1];
        yield return new WaitForSeconds(5f);
        this.GetComponent<SpriteRenderer>().sprite = sp[2];
        yield return new WaitForSeconds(5f);
        this.GetComponent<SpriteRenderer>().sprite = sp[3];
        this.gameObject.tag = "home";
        A.UpdateGraphs(GetComponent<CapsuleCollider2D>().bounds);
        StartCoroutine(coliderH());
        
    }
    //Hasta que no acaba el tiempo del upgrade, deja de ser util al cambiarle al tag y augmentamos la fase
    IEnumerator Upgrade1()
    {
        this.gameObject.tag = "time";
        yield return new WaitForSeconds(20f);
        this.GetComponent<SpriteRenderer>().sprite = sp[4];
        this.gameObject.tag = "home";
        fase = 2;
    }
    IEnumerator Upgrade2()
    {
        this.gameObject.tag = "time";
        yield return new WaitForSeconds(25f);
        this.GetComponent<SpriteRenderer>().sprite = sp[5];
        this.gameObject.tag = "home";
        fase = 3;
    }
    IEnumerator Upgrade3()
    {
        this.gameObject.tag = "time";
        yield return new WaitForSeconds(30f);
        this.GetComponent<SpriteRenderer>().sprite = sp[6];
        this.gameObject.tag = "home";
    }
    public void RebuildPath()
    {
        //Rescaner del A*
        A.Scan();
    }
    IEnumerator coliderH()
    {
        //Le ponemos el colider en trigger a la home
        yield return new WaitForSeconds(0.001f);
        this.gameObject.AddComponent<CircleCollider2D>();
        this.GetComponent<CircleCollider2D>().radius = 2.063193f;
        this.GetComponent<CircleCollider2D>().offset = new Vector2(0.2790562f, -0.7727718f);
        this.GetComponent<CircleCollider2D>().isTrigger = true;
    }

}
