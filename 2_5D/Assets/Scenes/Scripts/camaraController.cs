﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camaraController : MonoBehaviour
{

    /*Clase que controla la camara moviendo el raton hacia los margenes de la pantalla. Tambien esta programado el zoom*/

    float panSpeed = 1f;
    float scroll;



    // Start is called before the first frame update
    void Start()
    {
        this.transform.position = new Vector3(0,-7,-2);
    }

    // Update is called once per frame
    void Update()
    {
        //Mover camara hacia arriba
        if (Input.mousePosition.y >= Screen.height - 0.5f)
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y+panSpeed, this.transform.position.z);
        }

        //Mover camara hacia abajo
        if (Input.mousePosition.y <= 0.5f)
        {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y - panSpeed, this.transform.position.z);
        }

        //Mover camara hacia derecha
        if (Input.mousePosition.x >= Screen.width - 0.5f)
        {
            this.transform.position = new Vector3(this.transform.position.x + panSpeed, this.transform.position.y, this.transform.position.z);
        }

        //Mover camara hacia izquierda
        if (Input.mousePosition.x <= 0.5f)
        {
            this.transform.position = new Vector3(this.transform.position.x - panSpeed, this.transform.position.y , this.transform.position.z);
        }

        //Scroll
        scroll = Input.GetAxis("Mouse ScrollWheel");
        
        if (scroll>0 && this.GetComponent<Camera>().orthographicSize>1)
        {
            this.GetComponent<Camera>().orthographicSize -= 1;
        } else if (scroll<0 && this.GetComponent<Camera>().orthographicSize<=20)
        {
            this.GetComponent<Camera>().orthographicSize += 1;
        }
               
    }
}
