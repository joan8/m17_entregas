﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class werebearBehaviour : MonoBehaviour
{

    /*Clase del oso que esta en libertad. A diferencia del oso de las oleadas, este oso campa por el mapa esperando encontrar una victima y suelta carne*/

    //Cola en la cual se añaden las victimas del oso y se desencolan cuando mueren
    public Queue<GameObject> objetivos = new Queue<GameObject>();    
    

    //Estadisticas de werebear
    public float vida = 1;    
    float vel = 0.5f;

    //para perseguir
    bool objetivoDescubierto = false;
    public bool pillado = false;    

    
    //Otros componentes
    public Animator animator;
    public Image vidaOso;
    public int atacantes=0;
    private bool turnoOso=true;
    private bool coroutinDone = true;
    private bool encolado = false;
    
    
   //si un knight entra en el rango del oso, lo encola. Aunque solo puede encolar de uno en uno
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="knight" && !encolado){
            objetivoDescubierto = true;
            objetivos.Enqueue(collision.gameObject);
            encolado = true;
          
        }
        
    }

    private void OnTriggerStay2D(Collider2D collision) {


        //Condiciones para que el Oso persiga al jugador. Detecta sus coordenadas en cada frame.      
        if (objetivos.Count > 0)
        {
            if (this.transform.position.x >= objetivos.Peek().transform.position.x)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("walkRight", true);
                animator.SetBool("walkLeft", false);

            }
            else if (this.transform.position.x <= objetivos.Peek().transform.position.x)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("walkRight", true);
                animator.SetBool("walkLeft", false);

            }

            if (this.transform.position.y >= objetivos.Peek().transform.position.y)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
                animator.SetBool("walkLeft", true);
                animator.SetBool("walkRight", false);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
                animator.SetBool("walkLeft", false);
                animator.SetBool("walkRight", true);
            }
        } else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            animator.SetBool("walkLeft", false);
            animator.SetBool("walkRight", false);
        }     


    }
    //Si un knight muere o sale del area, el oso se detiene
    private void OnTriggerExit2D(Collider2D collision)
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
    }

    void Start()
    {
         
    }

    
    void Update()
    {
        
        //Aqui empieza el combate porque el oso ha alcanzado al objetivo. Si la cola no esta vacia, se inicia un combate por turnos con una corrutina.  
        if (pillado)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            animator.SetBool("walkLeft", false);
            animator.SetBool("walkRight", false);

            if (objetivos.Count > 0)
            {
                if (this.gameObject.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().fillAmount > 0 && objetivos.Peek().gameObject.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().fillAmount>0)
                {
                    if (turnoOso && coroutinDone)
                    {
                        objetivos.Peek().gameObject.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().fillAmount -= 0.5f;
                        if (objetivos.Peek().gameObject.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().fillAmount <= 0)
                        {
                            Destroy(objetivos.Peek().gameObject);
                            objetivos.Dequeue();
                            pillado = false;                          
                            this.gameObject.transform.GetChild(1).GetComponent<CircleCollider2D>().enabled = false;
                            this.gameObject.transform.GetChild(1).GetComponent<CircleCollider2D>().enabled = true;
                            encolado = false;
                            

                        }
                        coroutinDone = false;
                        StartCoroutine("turno");


                    }
                    else if (!turnoOso && coroutinDone)
                    {
                        this.gameObject.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().fillAmount -= 0.5f;
                        if (this.gameObject.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Image>().fillAmount <= 0)
                        {
                            
                            Destroy(this.gameObject);
                            objetivos.Peek().gameObject.transform.GetComponent<knightController>().carne += 300;
                            pillado = false;
                            
                        }
                        coroutinDone = false;
                        StartCoroutine("turno");
                    }
                }
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                turnoOso = true;
            }
        }
            

    }

    IEnumerator turno()
    {

        if (turnoOso)
        {            
            yield return new WaitForSeconds(1.5f);
            coroutinDone = true;
            turnoOso = false;

        } else
        {            
            yield return new WaitForSeconds(1.5f);
            coroutinDone = true;
            turnoOso = true;
        }

    }
}
