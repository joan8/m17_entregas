﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasMenu : MonoBehaviour
{
    private Home home;
    private CG cg;
    public GameObject mil;
    public GameObject casapref;
    public GameObject town;
    public GameObject aldeanoPref;
    public GameObject knightPref;
    private Control gameC;
    private Text carneT;
    private int carne;
    private Text maderaT;
    private int madera;
    private Text piedraT;
    private int piedra;
    private Text poblacionT;
    public int oleada { set; get; }
    public Text oleadaT { set; get; }
    private int poblacion;
    private int poblacionMax;
    private bool ciudad;
    public bool upHome { set; get; }
    public bool upCG { set; get; }

    // Start is called before the first frame update
    void Start()
    {
        gameC = GameObject.Find("GameControl").GetComponent<Control>();
        carneT = this.transform.GetChild(2).GetChild(0).GetChild(0).gameObject.GetComponent<Text>();
        carne = 100;
        carneT.text = carne + "";
        maderaT = this.transform.GetChild(2).GetChild(1).GetChild(0).gameObject.GetComponent<Text>();
        madera = 500;
        maderaT.text = madera + "";
        piedraT = this.transform.GetChild(2).GetChild(2).GetChild(0).gameObject.GetComponent<Text>();
        piedra = 250;
        piedraT.text = piedra + "";
        poblacionT = this.transform.GetChild(2).GetChild(3).GetChild(0).gameObject.GetComponent<Text>();
        poblacion = 1; 
        poblacionT.text = poblacion + "/" + poblacionMax;
        oleadaT = this.transform.GetChild(5).GetChild(0).GetChild(0).gameObject.GetComponent<Text>();
        oleada = 1;
        oleadaT.text = "Oleada: " + this.oleada;
        ciudad = false;
        poblacionMax = 2;
        InvokeRepeating("SetResources", 1, 5);
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void btnBuild()
    {
        //Activamos el menu de construcion
        this.transform.GetChild(1).gameObject.SetActive(true);
        this.transform.GetChild(0).gameObject.SetActive(false);
    }
    public void UpGradeTown()
    {
        //El funcionamiento es igual para todos los edificios y personas
        //si tienes lo recursos, se los restas al canvas y a la home, luego lo actualizas visualmente
        //En este caso especifico es para hacerle el Upgrade a la home
        if (this.madera >= 1000 && this.piedra >= 500)
        {
            this.madera -= 1000;
            this.piedra -= 500;
            home.madera -= 1000;
            home.stone -= 500;
            SetResources();
            upHome = true;
        }
    }
    public void UpGradeCG()
    {
        if (this.madera >= 500 && this.piedra >= 250)
        {
            this.madera -= 1000;
            this.piedra -= 500;
            home.madera -= 1000;
            home.stone -= 500;
            SetResources();
            upCG = true;
        }
    }
    public void Town()
    {
        //Este es un poco especia, porque solo deja crear un a home, es decir, si los enemigos llegan y la destruyen, se acaba el juego
        if (madera >= 500 && piedra >= 250 && !ciudad && GameObject.Find("TownPlace") == null)
        {
            this.madera -= 500;
            this.piedra -= 250;
            GameObject building = Instantiate(town);
            building.transform.parent = gameC.transform;
            StartCoroutine(StartHome());
            SetResources();
        }
    }

    public void crearHumano()
    {
        //Tambien hay que tener en cuenta el limite de poblacion, cada casa suma 2 habitante mas que puedes hacer
        if (50 <= carne && poblacion < poblacionMax)
        {
            this.carne -= 50;
            home.mead -= 50;
            SetResources();
            //Instanciamos el objecto y lo colocamos en la posicion de la home y-5, teniendo en cuenta el nivel de la home, contra mas alto, mas rapido ira la creacion del aldeano
            StartCoroutine(Alde());
        }
    }
    public void crearKnight()
    {
        if (100 <= carne && 25<= madera && poblacion < poblacionMax)
        {
            this.carne -= 50;
            home.mead -= 50;
            SetResources();
            this.madera -= 25;
            home.madera -= 25;
            //Instanciamos el objecto y lo colocamos en la posicion de la cuartel general y-5, teniendo en cuenta el nivel de la CG, contra mas alto, mas rapido ira la creacion del knight
            StartCoroutine(Knight());
           
        }
    }
    public void crearCasa()
    {
        if (50 <= madera && 50 <= piedra)
        {
            this.madera -= 50;
            this.piedra -= 50;
            home.madera -= 50;
            home.stone -= 20;
            GameObject a = Instantiate(casapref) as GameObject;
            if (home != null)
            {
                a.transform.position = new Vector3(home.transform.position.x, home.transform.position.y - 5, home.transform.position.z);
            }
            poblacionMax += 2;
            SetResources();
            this.transform.GetChild(1).gameObject.SetActive(false);
        }
    }
    public void CG()
    {
        if (madera >= 100 && piedra >= 50)
        {
            this.madera -= 100;
            this.piedra-= 50;
            home.stone -= 100;
            home.madera -= 50;

            GameObject building = Instantiate(mil);
            building.transform.parent = gameC.transform;
            if (home != null)
            {
                building.transform.position = new Vector3(home.transform.position.x, home.transform.position.y - 5, home.transform.position.z);
            }
            StartCoroutine(StartCG());
            SetResources();
            this.transform.GetChild(1).gameObject.SetActive(false);
        }
    }
    
    IEnumerator StartHome()
    {
        yield return new WaitForSeconds(0.5f);
        home = GameObject.Find("TownPlace").GetComponent<Home>();
        //Delegado para saber los recursos que adquiere el pueblo cuando los aldeanos o los knights lo llevan
        home.GetComponent<Home>().eventRes += Res;
        this.transform.GetChild(1).gameObject.SetActive(false);
        ciudad = true;
        home.madera -= 500;
        home.stone -= 250;
    }
    IEnumerator StartCG()
    {
        yield return new WaitForSeconds(0.5f);
        cg = GameObject.Find("CG").GetComponent<CG>();
        this.transform.GetChild(1).gameObject.SetActive(false);
    }
    IEnumerator Alde()
    {
        yield return new WaitForSeconds(5f / home.fase);
        GameObject a = Instantiate(aldeanoPref) as GameObject;
        a.transform.position = new Vector3(cg.transform.position.x, cg.transform.position.y - 2f, cg.transform.position.z);
        a.transform.parent = gameC.transform;
        poblacion++;
        this.transform.GetChild(3).gameObject.SetActive(false);
    }
    IEnumerator Knight()
    {
        yield return new WaitForSeconds(5f/cg.fase);
        GameObject a = Instantiate(knightPref) as GameObject;
        a.transform.position = new Vector3(cg.transform.position.x, cg.transform.position.y - 2f, cg.transform.position.z);
        a.transform.parent = gameC.transform;
        poblacion++;
        this.transform.GetChild(3).gameObject.SetActive(false);
    }
    public void Res(int carne, int madera,int stone)
    {
        this.carne = carne;
        this.madera = madera;
        this.piedra= stone;
        carneT.text = this.carne + "";
        maderaT.text = this.madera+"";
        piedraT.text = this.piedra+"";
    }
    public void SetResources()
    {
        carneT.text = this.carne + "";
        maderaT.text = this.madera + "";
        piedraT.text = this.piedra + "";
        poblacionT.text = this.poblacion + "/"+this.poblacionMax;
        oleadaT.text = "Oleada: "+this.oleada;
    }
}
