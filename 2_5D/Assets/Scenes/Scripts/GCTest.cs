﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GCTest : MonoBehaviour
{
    public List<GameObject> torres = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void suscribe()
    {
        foreach (GameObject t in torres)
        {
            t.transform.GetChild(0).GetComponent<AreaController>().killedAreaEvent += delete;
        }
    }

    void delete(GameObject enemigo)
    {
        foreach (GameObject t in torres)
        {
            if (t.transform.GetChild(0).GetComponent<AreaController>().colaTargets.Contains(enemigo))
            {
                t.transform.GetChild(0).GetComponent<AreaController>().colaTargets.Dequeue();
            }
        }
        Destroy(enemigo);
    }
}
