﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Palmera : MonoBehaviour
{
    public int madera { set; get; }
    // Start is called before the first frame update
    void Start()
    {
        //Le decimos la cantidad de recurso que tenemos y le ponemos el colider, para que el A* lo detecte
        this.madera = 100;
        StartCoroutine(coliderT());
    }

    // Update is called once per frame
    void Update()
    {
        if (madera <= 0)
        {
            Destroy(this.gameObject);
        }
    }
    IEnumerator coliderT()
    {
        yield return new WaitForSeconds(0.001f);
        this.gameObject.AddComponent<BoxCollider2D>();
        this.GetComponent<BoxCollider2D>().size = new Vector2(0.785347f, 1.273077f);
        this.GetComponent<BoxCollider2D>().offset = new Vector2(-0.00362289f, -0.8863658f);
        this.GetComponent<BoxCollider2D>().isTrigger = true;
    }
}
