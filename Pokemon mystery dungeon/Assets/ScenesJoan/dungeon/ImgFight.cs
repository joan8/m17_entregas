﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImgFight : MonoBehaviour
{
    public ImgFight img;
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }
        // Update is called once per frame
        void Update()
    {
        //Es la funcionalidad de la camera, pero para la imagen de groudon cuando entra dentro del rango.
        img.transform.position = new Vector3(
          player.transform.position.x,
          player.transform.position.y,
          img.transform.position.z);
    }
}
