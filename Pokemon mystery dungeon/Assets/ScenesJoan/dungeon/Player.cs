﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    //Variables para controlar el dinero del jugador, los triggers que activa, sus atributos etc
    public Animator animator;
    static GameObject squirtle = null;
    private Fight F;
    private bool trigger;
    private bool triggerN;
    private bool triggerG;
    public bool triggerEB { get; set; }
    public bool triggerMF { get; set; }
    private bool triggerparedf;
    public bool menuBool;
    private int anticLvl, nouLv;
    private int maxHp, exp;
    private float hp, ataque, defensa, level;
    public float vel;
    private float velinit ,velmax; 
    public float turbo {get; set;}
    private float turbomax;
    private static int dinero;
    private static int dineroBanco;



    // Awake is called on object instantiation
    void Awake()
    {
        //Singletone
        if (squirtle == null)
        {
           squirtle = this.gameObject;
            hp = 12;
            maxHp = 12;
            ataque = 6;
            defensa = 6;
            level = 1;
            DontDestroyOnLoad(squirtle);
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    // Start is called before the first frame update
    void Start()
    {

        //Variables de velocidad, posicion, atque,def...
        velinit = vel;
        velmax = vel * 2;
        turbomax = 50;
        turbo = turbomax;
        this.transform.position = new Vector3(0,0,0);
        PlayerF playerf = new PlayerF();
        F = GameObject.Find("GameControl").AddComponent<Fight>();
        playerf.setHP(this.getHp());
        playerf.setAtaque(this.getAtaque());
        playerf.setDefensa(this.getDefensa());
        playerf.setLv(this.getLv());
        animator = gameObject.GetComponent<Animator>();
        menuBool=false;
        exp = 0;
        anticLvl = (int)level;
        maxHp = (int)hp;
    }

    // Update is called once per frame
    void Update()
    {
      
        //Condicion al pulsar g, el jugador da un cabezazo que servira para romper algunas estructuras secretas
        if (Input.GetKeyDown("g"))
        {
            animator.SetBool("Cabeza", true);
            animator.SetBool("MoveD", false);
            animator.SetBool("MoveU", false);
            animator.SetBool("MoveR", false);
            animator.SetBool("MoveB", false);
        }else{
            animator.SetBool("Cabeza", false);
        }
        
        //Condiciones que generan movimiento
        if (Input.GetKey("d")) { 
          this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveR", true);
            animator.SetBool("MoveB", false);
        }
        else if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveB", true);
            animator.SetBool("MoveR", false);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveR", false);
            animator.SetBool("MoveB", false);
        }

        if (Input.GetKey("w"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel );
            animator.SetBool("MoveU", true);
            animator.SetBool("MoveD", false);
        }
        else if (Input.GetKey("s"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
            animator.SetBool("MoveD", true);
            animator.SetBool("MoveU", false);

        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
            animator.SetBool("MoveD", false);
            animator.SetBool("MoveU", false);

        }

        //Switch para subir de nivel
        if (Fight.final() || semilla())
        {
            switch (exp)
            {
                case 100:
                    nouLv = 2;
                    setEstats();
                    break;

                case 200:
                    nouLv = 3;
                    setEstats();
                    break;

                case 400:
                    nouLv = 4;
                    setEstats();
                    break;

                case 800:
                    nouLv = 5;
                    setEstats();
                    break;

                case 1500:
                    nouLv = 6;
                    setEstats();
                    break;

                case 2600:
                    nouLv = 7;
                    setEstats();
                    break;

                case 4200:
                    level = 8;
                    setEstats();
                    break;

                case 6400:
                    nouLv = 9;
                    setEstats();
                    break;

                case 9300:
                    nouLv = 10;
                    setEstats();
                    break;
            }
        }
       
        barraTurbo();
    }

    //Triggers que activan el encuentro con los enemigos y las escaleras que comunican las salas de la dungeon. Tambien estan los triggers de las estructuras secretas destruibles.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "t1En")
        {
            trigger = true;
        }

        if (collision.gameObject.tag == "t1Ex")
        {
            triggerN = true;
        }

        if (collision.tag == "bajar")
        {
            triggerEB = true;
        }
        else
        {
            triggerEB = false;
        }

        if (collision.gameObject.tag == "bajar2" || collision.gameObject.name == "bajarF")
        {
            triggerMF = true;
        }
        else
        {
            triggerMF = false;
        }

        if (collision.gameObject.tag == "t2En")
        {
            triggerG = true;
        }

        if (collision.gameObject.tag == "paredfalse")
        {
            triggerparedf = true;
        }

        //Collision que activa el recoger una moneda del suelo
        if (collision.gameObject.tag=="moneda")
        {
            dinero += 25;
            Destroy(collision.gameObject);
        }
  
    }

    //Al no estar en contacto con estos triggers, se desactiva la posibilidad de interactuar con ellos.
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "t1Ex")
        {
            trigger = false;
        }
          if (collision.gameObject.tag == "suelo")
        {
            triggerEB = false;
        }
          if (collision.gameObject.tag == "paredfalse")
        {
            triggerparedf = false;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

         

    }

    //Getters y setters
    public bool getTrigger()
    {
        return trigger;
    }
    public bool getTriggerN()
    {
        return triggerN;
    }
    public bool getTriggerEB(){
         return triggerEB;
    }
    public bool getTriggerMF()
    {
        return triggerMF;
    }
    public bool getTriggerG()
    {
        return triggerG;
    }
    public int getDinero()
    {
        return dinero;
    }
    public void setDinero(int din)
    {
        dinero = din;
    }

    public int getDineroBanco()
    {
        return dineroBanco;
    }

    public void setExp(int exp)
    {
        this.exp = exp;
    }

    public int getExp()
    {
        return exp;
    }

    public void setDineroBanco(int dinB)
    {
        dineroBanco = dinB;
    }
    public bool getParetF()
    {
        return triggerparedf;
    }
    public void setHP(float hp)
    {
        this.hp = hp;
    }
    public void augmentHP(float hp)
    {
        this.hp = hp+2;
    }
    public void setMaxHP(int maxHp)
    {
        this.maxHp = maxHp;
    }
    public void setAtaque(float ataque)
    {
        this.ataque = ataque+3;
    }
    public void setDefensa(float defensa)
    {
        this.defensa = defensa+2;
    }
    public void setLv(float level)
    {
        this.level = level+1;
    }
    public float getHp()
    {
        return hp;
    }
    public int getMaxHp()
    {
        return maxHp;
    }
    public float getAtaque()
    {
        return ataque;
    }
    public float getDefensa()
    {
        return defensa;
    }
    public float getLv()
    {
        return level;
    }
    public void setPosition(){
        this.transform.position=new Vector2(0,0);
    }
    public float getX()
    {
        return this.transform.position.x;
    }
    public float getY()
    {
        return this.transform.position.y;
    }

    //Funcion del turbo
    public void barraTurbo()
    {
        if (Input.GetKey("t"))
        {
            turbo--;

            if (turbo > 0)
            {
                vel = velmax;
            }
            if (turbo <= 0)
            {
                vel = velinit;
                turbo = 0;
            }
        }
        else
        {
             vel = velinit;
             if (turbo<100)
             {
                 turbo++;
             }   
             if (turbo == 100)
             {
                 turbo = 100;
             }
         }
         if (turbo > (turbomax * (0.75)))
         {
             this.transform.GetChild(0).GetChild(0).GetChild(4).gameObject.SetActive(true);
         }
         if (turbo < (turbomax * (0.75)))
         {
             this.transform.GetChild(0).GetChild(0).GetChild(4).gameObject.SetActive(false);
         }
         if (turbo > (turbomax * (0.5)))
         {
             this.transform.GetChild(0).GetChild(0).GetChild(3).gameObject.SetActive(true);
         }
         if (turbo <= (turbomax * (0.5)))
         {
             this.transform.GetChild(0).GetChild(0).GetChild(3).gameObject.SetActive(false);
         }
         if (turbo > (turbomax * (0.25)))
         {
            this.transform.GetChild(0).GetChild(0).GetChild(2).gameObject.SetActive(true);
         }
         if (turbo <= (turbomax * (0.25)))
         {
             this.transform.GetChild(0).GetChild(0).GetChild(2).gameObject.SetActive(false);
         }
         if (turbo <= 1)
         {
             this.transform.GetChild(0).GetChild(0).GetChild(1).gameObject.SetActive(false);
         }
         if (turbo > 1)
         {
             this.transform.GetChild(0).GetChild(0).GetChild(1).gameObject.SetActive(true);
         }
    }
    public void setEstats()
    {
        if (anticLvl != nouLv)
        {
            this.setLv(this.getLv());
            this.augmentHP(this.getHp());
            this.setAtaque(this.getAtaque());
            this.setDefensa(this.getDefensa());
            this.maxHp = (int)this.hp;

            finalF();
        }
    }

    private static void finalF()
    {
        Fight.setFinal();
    }
    public bool semilla()
    {
        if (Mochila.semilla())
        {
            exp = 100 + this.getExp();
            Mochila.semillaR(false);
            return true;
        }
        else
        {
            return false;
        }
    }
}
