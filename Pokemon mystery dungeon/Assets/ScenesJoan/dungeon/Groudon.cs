﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Groudon : MonoBehaviour
{
    public Animator animator;
    private GameObject Player;
    public Groudon Enemy;
    private Random Random = new Random();
    private bool trigger;
    private static bool triggerfight;
    private float DistX, DistY;
    public float MinDist;
    private float time, timeLimit = 72.0f;
    public int vel;
    private int move;
 
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.Find("Player");
        animator = gameObject.GetComponent<Animator>();
        InvokeRepeating("Hunt", 2, .5f);
    }

    // Update is called once per frame
    void Update()
    {

        //Atualizamos la posicion de groudon 
        Enemy.transform.position = new Vector3(Enemy.transform.position.x,
        Enemy.transform.position.y,
        Enemy.transform.position.z);

        //Calculo la distancia que hay entre el player i groudon
        DistX = Enemy.transform.position.x - Player.transform.position.x;
        DistY = Enemy.transform.position.y - Player.transform.position.y;

        //Si el Vector3 distancia entre los 2 es menos a 2 i llega al tiempo limite juntos, se activa la lucha
        if (Vector3.Distance(Enemy.transform.position, Player.transform.position) <= 2)
        {
            GameObject.Find("GameObject").transform.GetChild(0).gameObject.SetActive(true);
            GameObject.Find("GameObject").transform.GetChild(1).gameObject.SetActive(false);
            sumaTime();

            if (time==timeLimit) {
                triggerfight = true;
                SceneManager.LoadScene("GroudonFight");
            }
        }
    }

    void Hunt()
    {
        trigger = GameObject.Find("Player").GetComponent<Player>().getTriggerG();
        //Cuando esta a X de distancia, se "despiera i empieza a cazarte"
        if (!trigger)
        {
            animator.SetBool("Awake", false);
        }
        else
        {
            //Caluculando las distancia, se mueve de una manera o de otra, solo se mueve en diagonal, 
            //porque es muy dificil coincidir el awake, con la posicion 0.
            animator.SetBool("Awake", true);
            if (DistX <= MinDist)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("MoveR", true);
                animator.SetBool("MoveB", false);
            }
            else if (DistX >= MinDist)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                animator.SetBool("MoveB", true);
                animator.SetBool("MoveR", false);
            }
            else
            {
                animator.SetBool("MoveR", false);
                animator.SetBool("MoveB", false);
            }
            if (DistY <= MinDist)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
                animator.SetBool("MoveU", true);
                animator.SetBool("MoveD", false);

            }
            else if (DistY >= MinDist)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
                animator.SetBool("MoveD", true);
                animator.SetBool("MoveU", false);

            }
            else
            {
                animator.SetBool("MoveD", false);
                animator.SetBool("MoveU", false);

            }
            
        }
    }
    public void sumaTime()
    {
        time += 1;
    }
    public static bool getFight()
    {
        return triggerfight;
    }
    public static void setFight()
    {
        triggerfight = false;
    }
}
