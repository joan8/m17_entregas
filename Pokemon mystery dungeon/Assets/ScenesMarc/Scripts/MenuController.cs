﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
   
    public bool menuBool;
    public Text texto1, texto2, texto3;
    public Text valor1, valor2, valor3;
    public Text vida, nivel, dinero;
    public GameObject inv, columna, bas, men;
    private GameObject Player;


    static GameObject menu = null;

    void Awake()
    {
        if (menu == null)
        {
            menu = this.gameObject;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    void Start()
    {
        Player = GameObject.Find("Player");
        menuBool =false;
        this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
        this.gameObject.transform.GetChild(0).GetChild(2).gameObject.SetActive(false);
        texto1.text = "Baya Aranja";
        texto2.text = "Manzanita";
        texto3.text = "Semilla Dorada";
    }

    // Update is called once per frame
    void Update()
    {
        if ((SceneManager.GetActiveScene().name=="Carlos") || 
        (SceneManager.GetActiveScene().name=="Dungeon1") || 
        (SceneManager.GetActiveScene().name=="HotDungeon") ||
        (SceneManager.GetActiveScene().name=="Groudon")) {
            if (Input.GetKeyDown("i"))
            {
            activarMenu();
            }
        }
        else
        {
            menuBool = true;
            activarMenu();
        }
       
        vida.text = "Vida: " + Player.GetComponent<Player>().getHp().ToString() + "/" + Player.GetComponent<Player>().getMaxHp().ToString();
        nivel.text = "Nivel: " + Player.GetComponent<Player>().getLv().ToString() + " Exp: " +Player.GetComponent<Player>().getExp().ToString();
        dinero.text = "Dinero: " + Player.GetComponent<Player>().getDinero().ToString();
        if (Input.GetKeyDown("f"))
        {
            menuBool = true;
            activarMenu();
        }

        if (Mochila.inventario.ContainsKey("Baya Aranja"))
        {
            valor1.text = Mochila.inventario["Baya Aranja"].ToString();
        }
        else
        {
            valor1.text = "0";
        }

        if (Mochila.inventario.ContainsKey("Manzanita"))
        {
            valor2.text = Mochila.inventario["Manzanita"].ToString();
        }
        else
        {
            valor2.text = "0";
        }
        if (Mochila.inventario.ContainsKey("Semilla Dorada"))
        {
            valor3.text = Mochila.inventario["Semilla Dorada"].ToString();
        }
        else
        {
            valor3.text = "0";
        }
    }

    
    public void activarMenu()
    {
        if (!menuBool)
        {
            this.gameObject.transform.GetChild(0).gameObject.SetActive(true);
            this.gameObject.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
            this.gameObject.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
            this.gameObject.transform.GetChild(0).GetChild(2).gameObject.SetActive(true);
            menuBool =true;
        }
        else
        {
            this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
            menuBool =false;
        }
    }

    public void mostrarInventario()
    {
        this.gameObject.transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
        this.gameObject.transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
    }

    public void salir()
    {
        SceneManager.LoadScene("Inicio");
        this.gameObject.transform.GetChild(0).gameObject.SetActive(false);
    }

    public void volver()
    {
        this.gameObject.transform.GetChild(0).GetChild(0).gameObject.SetActive(true);
        this.gameObject.transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
    }
}
