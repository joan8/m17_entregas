﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemys : MonoBehaviour
{
    public int vel;
    private int move;
    public Animator animator;
    Random Random  = new Random();
    // Start is called before the first frame update
    void Start()
    {
         animator = gameObject.GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {

        move  = Random.Range(1, 81);
        Debug.Log(move);

         if (move<0 && move<=20)
         { 
          this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveR", true);
        }
        else if (move<20 && move<=40)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveB", true);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveR", false);
            animator.SetBool("MoveB", false);
        }
        if (move<40 && move<=60)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel );
            animator.SetBool("MoveU", true);

        }
        else if (move<60 && move<=80)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
            animator.SetBool("MoveD", true);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
            animator.SetBool("MoveD", false);
            animator.SetBool("MoveU", false);

        }
    }
}
