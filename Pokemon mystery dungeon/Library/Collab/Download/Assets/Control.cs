﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{

    public Player player;
    public Camera camera;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
         camera.transform.position = new Vector3(
            player.transform.position.x,
            player.transform.position.y,
            camera.transform.position.z);
    }
}
