﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private bool trigger;
    private bool triggerN;
    private bool triggerG;
    public bool triggerEB { get; set; }
    public bool triggerMF { get; set; }
    private bool triggerparedf;
    public bool menuBool;


    //Dinero del jugador
    private static int dinero;
    private static int dineroBanco;
    private int maxHp, exp;

    private float hp, ataque, defensa, level;

    public float vel;
    private float velinit ,velmax;
    public Animator animator;

    static GameObject squirtle = null;

    public float turbo {get; set;}
    private float turbomax;


    // Awake is called on object instantiation
    void Awake()
    {
        if (squirtle == null)
        {
           squirtle = this.gameObject;
            hp = 12;
            maxHp = 12;
            ataque = 6;
            defensa = 6;
            level = 1;
            DontDestroyOnLoad(squirtle);
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        velinit = vel;
        velmax = vel * 2;
        turbomax = 50;
        turbo = turbomax;
        this.transform.position = new Vector3(0,0,0);
        PlayerF playerf = new PlayerF();
        playerf.setHP(this.getHp());
        playerf.setAtaque(this.getAtaque());
        playerf.setDefensa(this.getDefensa());
        playerf.setLv(this.getLv());
        animator = gameObject.GetComponent<Animator>();
        menuBool=false;
    }

    // Update is called once per frame
    void Update()
    {
      
        if (Input.GetKeyDown("g"))
        {
            animator.SetBool("Cabeza", true);
            animator.SetBool("MoveD", false);
            animator.SetBool("MoveU", false);
            animator.SetBool("MoveR", false);
            animator.SetBool("MoveB", false);
        }else{
            animator.SetBool("Cabeza", false);
        }
        
        if (Input.GetKey("d")) { 
          this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveR", true);
            animator.SetBool("MoveB", false);
        }
        else if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveB", true);
            animator.SetBool("MoveR", false);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            animator.SetBool("MoveR", false);
            animator.SetBool("MoveB", false);
        }
        if (Input.GetKey("w"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel );
            animator.SetBool("MoveU", true);
            animator.SetBool("MoveD", false);
        }
        else if (Input.GetKey("s"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
            animator.SetBool("MoveD", true);
            animator.SetBool("MoveU", false);

        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
            animator.SetBool("MoveD", false);
            animator.SetBool("MoveU", false);

        }

        switch (exp)
        {
            case 0:
                level = 1;
                break;

            case 100:
                level = 2;
                break;

            case 200:
                level = 3;
                break;

            case 400:
                level = 4;
                break;

            case 800:
                level = 5;
                break;

            case 1500:
                level = 6;
                break;

            case 2600:
                level = 7;
                break;

            case 4200:
                level = 8;
                break;

            case 6400:
                level = 9;
                break;

            case 9300:
                level = 10;
                break;
        }
        barraTurbo();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "t1En")
        {
            trigger = true;
        }
        if (collision.gameObject.tag == "t1Ex")
        {
            triggerN = true;
        }
        if (collision.tag == "bajar")
        {
            triggerEB = true;
        }
        else
        {
            triggerEB = false;
        }
        if (collision.gameObject.tag == "bajar2" || collision.gameObject.name == "bajarF")
        {
            triggerMF = true;
        }
        else
        {
            triggerMF = false;
        }
        if (collision.gameObject.tag == "t2En")
        {
            triggerG = true;
        }
        if (collision.gameObject.tag == "paredfalse")
        {
            triggerparedf = true;
        }

        if (collision.gameObject.tag=="moneda")
        {
            dinero += 50;
            Destroy(collision.gameObject);
        }
  
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "t1Ex")
        {
            trigger = false;
        }
          if (collision.gameObject.tag == "suelo")
        {
            triggerEB = false;
        }
          if (collision.gameObject.tag == "paredfalse")
        {
            triggerparedf = false;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

         

    }

    public bool getTrigger()
    {
        return trigger;
    }
    public bool getTriggerN()
    {
        return triggerN;
    }
    public bool getTriggerEB(){
         return triggerEB;
    }
    public bool getTriggerMF()
    {
        return triggerMF;
    }
    public bool getTriggerG()
    {
        return triggerG;
    }
    public int getDinero()
    {
        return dinero;
    }
    public void setDinero(int din)
    {
        dinero = din;
    }

    public int getDineroBanco()
    {
        return dineroBanco;
    }

    public void setExp(int exp)
    {
        this.exp = exp;
    }

    public int getExp()
    {
        return exp;
    }

    public void setDineroBanco(int dinB)
    {
        dineroBanco = dinB;
    }
    public bool getParetF()
    {
        return triggerparedf;
    }
    public void setHP(float hp)
    {
        this.hp = hp;
    }
    public void setMaxHP(int maxHp)
    {
        this.maxHp = maxHp;
    }
    public void setAtaque(float ataque)
    {
        this.ataque = ataque;
    }
    public void setDefensa(float defensa)
    {
        this.defensa = defensa;
    }
    public void setLv(float level)
    {
        this.level = level;
    }
    public float getHp()
    {
        return hp;
    }
    public int getMaxHp()
    {
        return maxHp;
    }
    public float getAtaque()
    {
        return ataque;
    }
    public float getDefensa()
    {
        return defensa;
    }
    public float getLv()
    {
        return level;
    }
    public void setPosition(){
        this.transform.position=new Vector2(0,0);
    }
    public float getX()
    {
        return this.transform.position.x;
    }
    public float getY()
    {
        return this.transform.position.y;
    }
    public void barraTurbo()
    {
        if (Input.GetKey("t"))
        {
            turbo--;
            Debug.Log(turbo);

            if (turbo > 0)
            {
                vel = velmax;
            }
            if (turbo <= 0)
            {
                vel = velinit;
                turbo = 0;
            }
        }
        else
        {
             vel = velinit;
             if (turbo<100)
             {
                 turbo++;
             }   
             if (turbo == 100)
             {
                 turbo = 100;
             }
         }
         if (turbo > (turbomax * (0.75)))
         {
             this.transform.GetChild(0).GetChild(0).GetChild(4).gameObject.SetActive(true);
         }
         if (turbo < (turbomax * (0.75)))
         {
             this.transform.GetChild(0).GetChild(0).GetChild(4).gameObject.SetActive(false);
         }
         if (turbo > (turbomax * (0.5)))
         {
             this.transform.GetChild(0).GetChild(0).GetChild(3).gameObject.SetActive(true);
         }
         if (turbo <= (turbomax * (0.5)))
         {
             this.transform.GetChild(0).GetChild(0).GetChild(3).gameObject.SetActive(false);
         }
         if (turbo > (turbomax * (0.25)))
         {
            this.transform.GetChild(0).GetChild(0).GetChild(2).gameObject.SetActive(true);
         }
         if (turbo <= (turbomax * (0.25)))
         {
             this.transform.GetChild(0).GetChild(0).GetChild(2).gameObject.SetActive(false);
         }
         if (turbo <= 1)
         {
             this.transform.GetChild(0).GetChild(0).GetChild(1).gameObject.SetActive(false);
         }
         if (turbo > 1)
         {
             this.transform.GetChild(0).GetChild(0).GetChild(1).gameObject.SetActive(true);
         }
    }
}
