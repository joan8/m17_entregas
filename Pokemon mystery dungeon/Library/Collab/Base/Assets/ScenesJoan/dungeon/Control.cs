﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class Control : MonoBehaviour
{

    //Atributos de ataque,defensa...
    float atck, def, lvl;



    private bool triggerH, triggerF;
   // public Player player;
    public Camera camera;

    public GameObject player;
    
   

    //Botones para el menu banco
    public InputField campoDinero;
    public bool depositando = false;
    


    //Imagenes para el avatar segun con quien hables
    public Sprite avatarAmpharos;
    public Sprite avatarOnyx;   
    public Sprite avatarKangaskhan;

    //Para activar las conversaciones;
    public Canvas ventanaDialogo;
    public Text textoDialogo;

    //Canvas de la tienda
    public Canvas canvasTienda, Turbito;
    public Text textoTienda;
    public Text textoCantidadTienda;
    public bool sand { set; get; }
    public bool nido { set; get; }
    public static bool locaD { set; get; }
    public static bool locaH { set; get; }

    private bool destS;

    void OnAwake(){

        player=GameObject.Find("Player");
        player.AddComponent<Player>();
   
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        //Debug.Log("Awake:" + SceneManager.GetActiveScene().name);


        if (SceneManager.GetActiveScene().name=="Carlos"){
            player.transform.position = new Vector2(0,0);
            locaD = false;
            locaH = false;
            
        }

        /*if (SceneManager.GetActiveScene().name == "Dungeon1")
        {
            if (sand)
            {
                destS = true;
            }
            else
            {
                GameObject.Find("Pokemons").transform.GetChild(0).gameObject.SetActive(true);
            }

        }*/

        if (SceneManager.GetActiveScene().name == "Carlos")
        {
            ventanaDialogo.GetComponent<Canvas>().enabled = false;
            canvasTienda.GetComponent<Canvas>().enabled = false;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (destS)
        {
            Destroy(GameObject.Find("Sandshrew"));
            //destS = false;
        }
        else
        {

        }

        triggerH = GameObject.Find("Player").GetComponent<Player>().getTriggerEB();
        triggerF = GameObject.Find("Player").GetComponent<Player>().getTriggerMF();

        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene("Groudon");
        }
        else
        {
        }

        if (triggerH)
        {
            GameObject.Find("Player").GetComponent<Player>().triggerEB = false;
            locaH = true;
            SceneManager.LoadScene("HotDungeon");
        }
        if (triggerF)
        {
            GameObject.Find("Player").GetComponent<Player>().triggerMF=false;
            GameObject.Find("Player").transform.position = new Vector3(104.7f,-40.7f,0); 
            SceneManager.LoadScene("Groudon");
        }



        //Dialogo con los NPC
        if (Dialogos.hablandoAmpharos)
        {
            if (Input.GetKeyDown("f"))
            {
                
                ventanaDialogo.GetComponent<Canvas>().enabled = true;
                ventanaDialogo.transform.GetChild(2).gameObject.SetActive(true);
                ventanaDialogo.transform.GetChild(1).gameObject.SetActive(true);
                ventanaDialogo.transform.GetChild(1).gameObject.GetComponent<Image>().sprite = avatarAmpharos;
                textoDialogo.text = "Ampharos: Bienvenidos a la banca etica! Aqui no invertimos en Pokeguerras." +
                    "Tienes " + GameObject.Find("Player").GetComponent<Player>().getDineroBanco() + " pokemoneda/s en el banco. Quires depositar o retirar dinero? y "+ GameObject.Find("Player").GetComponent<Player>().getDinero() + " en tu bolsillo";
            }
        }
        else if (Dialogos.hablandoOnyx)
        {
            if (Input.GetKeyDown("f"))
            {
                ventanaDialogo.GetComponent<Canvas>().enabled = true;
                ventanaDialogo.transform.GetChild(1).gameObject.SetActive(true);
                ventanaDialogo.transform.GetChild(1).gameObject.GetComponent<Image>().sprite = avatarOnyx;
                textoDialogo.text = "Onyxillo: Que pasa mariconas? Recordad no tener relaciones con Pokemons del mismo genero";
            }
        }
        else if (Dialogos.hablandoGrovyle)
        {
            textoCantidadTienda.text=""+ GameObject.Find("Player").GetComponent<Player>().getDinero();
            if (Input.GetKeyDown("f"))
            {                
                canvasTienda.GetComponent<Canvas>().enabled=true;               
                textoTienda.text="Rubyle: Shh... Que deseas comprar?";
            }
        }
        else if (Dialogos.hablandoKangaskhan)
        {
            if (Input.GetKeyDown("f"))
            {
                ventanaDialogo.GetComponent<Canvas>().enabled = true;
                ventanaDialogo.transform.GetChild(1).gameObject.SetActive(true);
                ventanaDialogo.transform.GetChild(1).gameObject.GetComponent<Image>().sprite = avatarKangaskhan;
                textoDialogo.text = "Clemenskhan: Que duro es ser padre soltero... Bienvenido al deposito de objetos";
            }
        }
        else if (Dialogos.entradaDungeon)
        {
            //pulsar f porque si no, al volver a cargar la escena, aparecia siempre
            if (Input.GetKeyDown("f"))
            {
                ventanaDialogo.GetComponent<Canvas>().enabled = true;
                ventanaDialogo.transform.GetChild(1).gameObject.SetActive(false);
                ventanaDialogo.transform.GetChild(3).gameObject.SetActive(true);
                textoDialogo.text = "Deseas entrar a la mazmorra?";
            }
            
        } else if (Dialogos.puntoGuardado)
        {
            if (Input.GetKeyDown("f"))
            {
                ventanaDialogo.GetComponent<Canvas>().enabled = true;
                ventanaDialogo.transform.GetChild(1).gameObject.SetActive(false);
                ventanaDialogo.transform.GetChild(7).gameObject.SetActive(true);
                textoDialogo.text = "Deseas guardar partida?";
            }  

        }else
        {
            //Else para opcion Banco
            if(SceneManager.GetActiveScene().name == "Carlos")
            {
                ventanaDialogo.GetComponent<Canvas>().enabled = false;
                ventanaDialogo.transform.GetChild(2).gameObject.SetActive(false);
                ventanaDialogo.transform.GetChild(1).gameObject.SetActive(false);
                ventanaDialogo.transform.GetChild(3).gameObject.SetActive(false);
                ventanaDialogo.transform.GetChild(4).gameObject.SetActive(false);
                ventanaDialogo.transform.GetChild(7).gameObject.SetActive(false);
                //Else para tienda
                canvasTienda.GetComponent<Canvas>().enabled = false;
            }
           
        }
    }

    //Guardar y cargar partida
    private Save createSave()
    {
        Save save = new Save();
        
        save.dineroBancoSave = GameObject.Find("Player").GetComponent<Player>().getDineroBanco();
        save.dineroPlayerSave =  GameObject.Find("Player").GetComponent<Player>().getDinero();
        save.hpS = GameObject.Find("Player").GetComponent<Player>().getHp();
        save.ataqueS = GameObject.Find("Player").GetComponent<Player>().getAtaque();
        save.defensaS = GameObject.Find("Player").GetComponent<Player>().getDefensa();
        save.levelS = GameObject.Find("Player").GetComponent<Player>().getLv();
        return save;
    }

    public void saveGame()
    {
        //crees el objecte save
        Save save = this.createSave();
        //crees un BinaryFormatter que es com un OOS
        BinaryFormatter bf = new BinaryFormatter();
        //Crees el File
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");        
        //serialitzes
        bf.Serialize(file, save);
        file.Close();

        Debug.Log("Game Saved");
    }

    public static void LoadGame()
    {
        // 1
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {

            // 2
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Save save = (Save)bf.Deserialize(file);
            file.Close();
            // 3

            GameObject.Find("Player").GetComponent<Player>().setDinero(save.dineroPlayerSave);
            GameObject.Find("Player").GetComponent<Player>().setDineroBanco(save.dineroBancoSave);
            GameObject.Find("Player").GetComponent<Player>().setHP(save.hpS);
            GameObject.Find("Player").GetComponent<Player>().setAtaque(save.ataqueS);
            GameObject.Find("Player").GetComponent<Player>().setDefensa(save.defensaS);
            GameObject.Find("Player").GetComponent<Player>().setLv(save.levelS);


            Debug.Log("Game Loaded");

        }
        else
        {
            Debug.Log("No game saved!");
        }
    }


        public void retirarDineroDialogo()
    {
        textoDialogo.text = "Cuanto quieres retirar? Tienes " + GameObject.Find("Player").GetComponent<Player>().getDineroBanco() + " monedas ahorradas.";
        ventanaDialogo.transform.GetChild(2).gameObject.SetActive(false);
        ventanaDialogo.transform.GetChild(4).gameObject.SetActive(true);
        ventanaDialogo.transform.GetChild(5).gameObject.SetActive(false);
        ventanaDialogo.transform.GetChild(6).gameObject.SetActive(true);

    }

    public void depositarDineroDialogo()
    {
        textoDialogo.text = "Cuanto quieres depositar? Tienes " + GameObject.Find("Player").GetComponent<Player>().getDineroBanco() + " monedas ahorradas.";
        ventanaDialogo.transform.GetChild(2).gameObject.SetActive(false);
        ventanaDialogo.transform.GetChild(4).gameObject.SetActive(true);
        ventanaDialogo.transform.GetChild(5).gameObject.SetActive(true);
        ventanaDialogo.transform.GetChild(6).gameObject.SetActive(false);


    }
    public void depositarDinero()
    {

        if (GameObject.Find("Player").GetComponent<Player>().getDinero() >= int.Parse(campoDinero.text) && int.Parse(campoDinero.text) != 0)
        {
            GameObject.Find("Player").GetComponent<Player>().setDinero(GameObject.Find("Player").GetComponent<Player>().getDinero() - int.Parse(campoDinero.text));
            GameObject.Find("Player").GetComponent<Player>().setDineroBanco(GameObject.Find("Player").GetComponent<Player>().getDineroBanco() + int.Parse(campoDinero.text));

            ventanaDialogo.transform.GetChild(4).gameObject.SetActive(false);
            ventanaDialogo.transform.GetChild(2).gameObject.SetActive(true);
            textoDialogo.text = "Genial! tu dinero irá a bienes e inmuebles de la banca etica! Gracias por tu confianza!";

        }
        else
        {
            ventanaDialogo.transform.GetChild(4).gameObject.SetActive(false);
            ventanaDialogo.transform.GetChild(2).gameObject.SetActive(true);
            textoDialogo.text = "Lo siento, no puedes depositar esa cantidad de dinero";

        }
        campoDinero.text = "";
        ventanaDialogo.transform.GetChild(5).gameObject.SetActive(false);
        ventanaDialogo.transform.GetChild(6).gameObject.SetActive(false);


    }
    public void retirarDinero()
    {

        if (GameObject.Find("Player").GetComponent<Player>().getDineroBanco() >= int.Parse(campoDinero.text) && int.Parse(campoDinero.text)!=0)
        {
            GameObject.Find("Player").GetComponent<Player>().setDinero(GameObject.Find("Player").GetComponent<Player>().getDinero() + int.Parse(campoDinero.text));
            GameObject.Find("Player").GetComponent<Player>().setDineroBanco(GameObject.Find("Player").GetComponent<Player>().getDineroBanco() - int.Parse(campoDinero.text));
            
            
            ventanaDialogo.transform.GetChild(4).gameObject.SetActive(false);
            ventanaDialogo.transform.GetChild(2).gameObject.SetActive(true);
            textoDialogo.text = "Genial! tu retirada ha sido satisfactoria.";

        }
        else
        {
            ventanaDialogo.transform.GetChild(4).gameObject.SetActive(false);
            ventanaDialogo.transform.GetChild(2).gameObject.SetActive(true);
            textoDialogo.text = "Lo siento, no puedes retirar esa cantidad de dinero";
            
        }
        campoDinero.text = "";
        ventanaDialogo.transform.GetChild(5).gameObject.SetActive(false);
        ventanaDialogo.transform.GetChild(6).gameObject.SetActive(false);
    }

    public void entrarMazmorra()
    {
        Dialogos.entradaDungeon = false;
        locaD = true;
        SceneManager.LoadScene("Dungeon1");

        
    }
      
}