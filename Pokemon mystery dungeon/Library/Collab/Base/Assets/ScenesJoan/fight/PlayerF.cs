﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerF : MonoBehaviour
{
    private static float hp = 12, ataque = 6, defensa = 6, level = 1;
    private string atac1 = "Arañazo", atac2 = "Burbuja", atac3 = "Hidro Bomba", atac4 = "Surf";
    private int arapp = 35, burbpp = 25, hidropp = 5, surfpp = 15;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public float gethp()
    {
        return hp;
    }
    public float getLv()
    {
        return level;
    }
    public float getatac()
    {
        return ataque;
    }
    public float getdef()
    {
        return defensa;
    }
    public string getatac1()
    {
        return atac1;
    }
    public string getatac2()
    {
        return atac2;
    }
    public string getatac3()
    {
        return atac3;
    }
    public string getatac4()
    {
        return atac4;
    }
    public int gethidro()
    {
        return hidropp;
    }
    public int getarañazo()
    {
        return arapp;
    }
    public int getsurf()
    {
        return surfpp;
    }
    public int getburbuja()
    {
        return burbpp;
    }
}
