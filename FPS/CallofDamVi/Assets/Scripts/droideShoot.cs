﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class droideShoot : MonoBehaviour
{
    public Camera cam;
    public GameObject impacto;
    private bool disparado;
    private int rango;
    // Start is called before the first frame update
    void Start()
    {
        rango = 35;
    }

    // Update is called once per frame
    void Update()
    {
        disparado = false;
        StartCoroutine(Shot());
    }
    IEnumerator Shot()
    {
        yield return new WaitForSeconds(0.3f);

        RaycastHit hit;


        int layerMask = 1 << 9;
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, rango, layerMask))
        {
            //Agafar el Vector 3 de la camera
            Vector3 forward = cam.transform.TransformDirection(Vector3.forward) * 100000000;
            //Dibuixar el raig del RayCast
            Debug.DrawRay(transform.position, forward, Color.red);
            //Instancia el dispar
            if (!disparado)
            {
                //Agafa que el target es el player
                Life player = hit.transform.GetComponent<Life>();

                GameObject shoot = Instantiate(impacto, hit.point, Quaternion.LookRotation(hit.normal));
                shoot.transform.LookAt(hit.point);
                //audio
                print(hit.transform.name);
                if (player != null)
                {
                    print(player.hp);
                    player.hp -= 10;
                }
                disparado = true;
            }
        }
        StopAllCoroutines();
    }
}

