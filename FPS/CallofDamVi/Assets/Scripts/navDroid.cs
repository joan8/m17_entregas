﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class navDroid : MonoBehaviour
{
    public NavMeshAgent agent;
    public Droid droid;
    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("RigidBodyFPSController"); 
    }

    // Update is called once per frame
    void Update()
    {
        if (0 < droid.hp) {
            this.transform.LookAt(player.transform);
            agent.SetDestination(player.transform.position);
        }
        else
        {
            this.agent.enabled = false;
        }
    }
}
