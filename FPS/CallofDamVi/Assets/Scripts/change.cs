﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class change : MonoBehaviour
{
    private Vector3 pos;
    private GameObject helis;
    public GameObject player;
    private Vector3 old;
    // Start is called before the first frame update
    void Start()
    {
        helis = GameObject.Find("Helis");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag =="player")
        {
            old = player.transform.position;
            helis.transform.GetChild(1).gameObject.SetActive(true);
            helis.transform.GetChild(0).gameObject.SetActive(false);
            pos = this.transform.parent.transform.GetChild(5).transform.position;
            this.transform.parent.transform.GetChild(5).gameObject.SetActive(false);
            this.transform.GetComponent<Collider>().enabled = false;
            StartCoroutine(final());
        }
    }
    IEnumerator final()
    {
        yield return new WaitForSeconds(30);
        player.transform.position = old;
        helis.transform.GetChild(1).gameObject.SetActive(false);
        this.transform.parent.transform.GetChild(5).gameObject.SetActive(true);
        this.transform.parent.transform.GetChild(5).transform.position = pos;
    }
}
