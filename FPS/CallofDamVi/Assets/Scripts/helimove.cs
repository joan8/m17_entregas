﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class helimove : MonoBehaviour
{
    public int ROTATE_SPEED;
    public int SPEED;
    public float frenada;
    public GameObject cam;
    private Quaternion old;
    private float altura;
    private float forceUp;
    private float forceDown;
    private bool empezar;
    private bool ready;
    // Start is called before the first frame update
    void Start()
    {
        old = new Quaternion(0,0,0,this.transform.localRotation.w);
        altura = this.transform.position.y;
        forceUp = 15000;
        forceDown = 1000;
        empezar = false;
        ready = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (ready)
        {
            StartCoroutine(STARTC());
            StartCoroutine(STARTfly());
        }
        

        if (Input.GetKey("w"))
        {
            if (frenada >= 0.05 *SPEED)
            {
                frenada -= 0.1f;
            }
        }
        if (Input.GetKey("s"))
        {
            if (frenada <= 0.99 * SPEED)
            {
               
                frenada += 0.1f;               
            }
          
        }
        if (Input.GetKey("a"))
        {
            this.transform.parent.transform.RotateAround(transform.position, transform.up, Time.deltaTime * -ROTATE_SPEED);
        }
        else if (Input.GetKey("d"))
        {
            this.transform.parent.transform.RotateAround(transform.position, transform.up, Time.deltaTime * ROTATE_SPEED);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            ready = true;
            forceUp += 2000f;
            this.transform.GetComponent<Rigidbody>().AddForce(0, forceUp, 0);
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            forceDown += 2000;
            this.transform.GetComponent<Rigidbody>().AddForce(0, -forceDown, 0);
        }
        else
        {
            if (empezar) {
                if (15000 < forceUp)
                {
                    forceUp -= 1000f;
                }
                forceDown = 0;
            }
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.parent.transform.RotateAround(transform.position, transform.forward, Time.deltaTime * -ROTATE_SPEED);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.parent.transform.RotateAround(transform.position, transform.forward, Time.deltaTime * ROTATE_SPEED);
        }
        


    }
    IEnumerator STARTC()
    {
        yield return new WaitForSeconds(1.15f);
        this.GetComponent<Rigidbody>().velocity = cam.transform.forward * (SPEED / frenada);
    }
    IEnumerator STARTfly()
    {
        yield return new WaitForSeconds(1.15f);
        empezar = true;
    }
    private void OnCollisionEnter(Collision collision)
    {
        this.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
    }
}
