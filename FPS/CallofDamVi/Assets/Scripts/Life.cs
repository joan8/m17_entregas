﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Life : MonoBehaviour
{
    public Text txtlife;

    public int hp { set; get; }
    // Start is called before the first frame update
    void Start()
    {
        hp = 100;
        txtlife.text = hp.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        txtlife.text = hp.ToString();


        if (hp <= 0 )
        {
            //End game
            Destroy(this.gameObject);
        }
    }
}
