﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class apuntar : MonoBehaviour
{
    private Vector3 origen;
    public Camera normal;
    public Camera shooting;
    private bool apuntando = false;
    private float ZoomSpeed = 80f;

    public float currentFOV;
    // Start is called before the first frame update
    void Start()
    {
        origen = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y, this.transform.localPosition.z);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1))
        {
            this.transform.localPosition = new Vector3(0.02f, -0.1f, 0.07f);
            if (!apuntando)
            {
                StartCoroutine(apuntarArma());
            }

        }
        else if (Input.GetMouseButtonUp(1))
        {
            apuntando = false;
            this.transform.localPosition = origen;
            shooting.enabled = false;
            normal.enabled = true;
            this.GetComponent<MeshRenderer>().enabled = true;
        }
        if (Input.GetMouseButtonDown(0) && !apuntando)
        {
            this.transform.GetChild(0).gameObject.SetActive(true);
        }
        if (Input.GetMouseButtonUp(0))
        {
            this.transform.GetChild(0).gameObject.SetActive(false);
        }



        //Fov actual
        currentFOV = shooting.fieldOfView;
       // Debug.Log(currentFOV);

        //Scroll mientras se apunta
        var d = Input.GetAxis("Mouse ScrollWheel");
        if (d > 0f)
        {
            //Zoom out
            if (shooting.gameObject.GetComponent<Camera>().fieldOfView > 2)
            {
                shooting.gameObject.GetComponent<Camera>().fieldOfView -= 2f;
                //shooting.fieldOfView -= Mathf.Lerp(currentFOV, d, ZoomSpeed);
            }

        }
        else if (d < 0f)
        {
            //Zoom in
            if (shooting.gameObject.GetComponent<Camera>().fieldOfView < 20)
            {
                //shooting.fieldOfView += Mathf.Lerp(currentFOV, d, ZoomSpeed);
                shooting.gameObject.GetComponent<Camera>().fieldOfView += 2f;
            }
        }

        
    }
    IEnumerator apuntarArma()
    {
        yield return new WaitForSeconds(0.1f);
        shooting.enabled = true;
        normal.enabled = false;
        apuntando = true;
        this.GetComponent<MeshRenderer>().enabled = false;
        StopAllCoroutines();
    }

}
