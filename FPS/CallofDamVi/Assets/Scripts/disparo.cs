﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class disparo : MonoBehaviour
{
    public Camera cam;
    public GameObject impacto;
    public GameObject fuego;
    private int daño = 500;
    public Canvas canvas;    
    private int rango;
    private float cad;
    private bool disparado;
    private bool start;

    //Audios
    private int audio;
    public AudioClip l96;
    public AudioClip Ak47;
    //public AudioClip Pistola;
    public AudioClip reloadAudio;


    //Municion
    int balasL96;
    int balasAK;
    int balasPistola;
    //Cargador
    int cargL96;
    int cargAK;
    int cargPistola;
    //Total
    int balasTotalL96;
    int balasTotalAK;
    int balasTotalPistola;

    //Texto de municion
    public Text txtBalas;

    // Start is called before the first frame update
    void Start()
    {
        rango = 0;
        cad = 0;
        disparado = false;
        start = true;

        //TODO Hacerlo scriptable object puto andreu
        //Municion
        balasL96=10;
        balasAK=30;
        balasPistola=6;
        //Cargador
        cargL96 = 10;
        cargAK = 30;
        cargPistola = 6;
        //Total
        balasTotalL96 =10;
        balasTotalAK=150;
        balasTotalPistola=17;
    }

    
    

    // Update is called once per frame
    void Update()
    {
        //Elegir el arma
        if (Input.GetKeyDown("1")) //L96
        {
            transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(false);
            
        }
        else if(Input.GetKeyDown("2")) //Pistola
        { 
            transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(true);
            transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(false);
            
        }
        else if (Input.GetKeyDown("3")) //AK
        {
            transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(true);
            transform.GetChild(0).transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(0).transform.GetChild(0).gameObject.SetActive(false);
            
        }
        
        
        //Cambiar los valores de daño,rango.. segun el arma
        if (transform.GetChild(0).transform.GetChild(0).gameObject.active == true) //L96
        {
            daño = 500;
            rango = 400;
            cad = 1.5f;
            this.transform.GetChild(2).GetComponent<AudioSource>().clip = l96;
            txtBalas.text = balasL96.ToString() + " / " + balasTotalL96.ToString();
        }
        else if (transform.transform.GetChild(0).GetChild(1).gameObject.active == true) //Pistola
        {
            daño = 50;
            rango = 25;
            cad = 0.4f;
            txtBalas.text = balasPistola.ToString() + " / " + balasTotalPistola.ToString();
        }
        else if (transform.transform.GetChild(0).GetChild(2).gameObject.active == true) //AK
        {
            daño = 150;
            rango = 60;
            cad = 0.05f;
            this.transform.GetChild(2).GetComponent<AudioSource>().clip = Ak47;
            txtBalas.text = balasAK.ToString() + " / " + balasTotalAK.ToString();
        }

        //print(daño);



        //Disparar. Si hay balas disponibles
        if (Input.GetMouseButton(0))
        {
            if (transform.GetChild(0).transform.GetChild(0).gameObject.active == true && balasL96>0) //L96
            {
                if (start)
                {
                    StartCoroutine(cadencia(0));
                    start = false;
                }
                else
                {
                    StartCoroutine(cadencia(cad));
                }
            }
            else if (transform.transform.GetChild(0).GetChild(1).gameObject.active == true && balasPistola>0) //Pistola
            {
                if (start)
                {
                    StartCoroutine(cadencia(0));
                    start = false;
                }
                else
                {
                    StartCoroutine(cadencia(cad));
                }
            }
            else if (transform.transform.GetChild(0).GetChild(2).gameObject.active == true && balasAK>0) //AK
            {
                if (start)
                {
                    transform.transform.GetChild(0).GetChild(2).transform.GetChild(0).gameObject.SetActive(true);
                    StartCoroutine(cadencia(0));
                    start = false;
                }
                else
                {
                    transform.transform.GetChild(0).GetChild(2).transform.GetChild(0).gameObject.SetActive(true);
                    StartCoroutine(cadencia(cad));

                }
            }

            
        }
        else if (Input.GetKeyDown("r"))
        {
            
            StartCoroutine(reload());

        }else if (Input.GetMouseButtonUp(0))
        {
            start = true;
            StopAllCoroutines();
        }

      

    }

    //Recargar
    IEnumerator reload()
    {
        
        yield return new WaitForSeconds(0.5f);
        if (transform.GetChild(0).transform.GetChild(0).gameObject.active == true) //L96
        {
            //El cargador de la L96 es unico           
        }
        else if (transform.transform.GetChild(0).GetChild(1).gameObject.active == true) //Pistola
        {                   

            if (balasTotalPistola>=(cargPistola-balasPistola))
            {
                balasTotalPistola -= cargPistola-balasPistola;
                balasPistola = cargPistola;
            } else
            {
                balasPistola += balasTotalPistola;
                balasTotalPistola = 0;
            }
           
        }
        else if (transform.transform.GetChild(0).GetChild(2).gameObject.active == true) //AK
        {

            if (balasTotalAK >= (cargAK - balasAK))
            {
                balasTotalAK -= cargAK - balasAK;
                balasAK = cargAK;
            }
            else
            {
                balasAK += balasTotalAK;
                balasTotalAK = 0;
            }
        }
    }

    IEnumerator cadencia(float time)
    {
        yield return new WaitForSeconds(time);
        RaycastHit hit;


        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, rango))
        {
            //Instancia el dispar
            if (!disparado)
            {
                //Agafa que el target es el player
                barril barrilete = hit.transform.GetComponent<barril>();
                Droid droide = hit.transform.GetComponent<Droid>();
                heli Heli = hit.transform.GetComponent<heli>();

                GameObject shoot = Instantiate(impacto, hit.point, Quaternion.LookRotation(hit.normal));
                shoot.transform.LookAt(hit.point);
                shoot.GetComponent<impactoScript>().audios = this.transform.GetChild(2).GetComponent<AudioSource>().clip;

                //Restar municion
                if (transform.GetChild(0).transform.GetChild(0).gameObject.active == true) //L96
                {
                    if (balasL96>0)
                    {
                        balasL96 -= 1;
                    }
                    
                }
                else if (transform.transform.GetChild(0).GetChild(1).gameObject.active == true) //Pistola
                {
                    if (balasPistola>0)
                    {
                        balasPistola -= 1;

                    }
                }
                else if (transform.transform.GetChild(0).GetChild(2).gameObject.active == true) //AK
                {
                    if (balasAK>0)
                    {
                        balasAK -= 1;
                    }
                    
                }

                if (barrilete != null)
                {
                    //Baixa la vida del player
                    barrilete.gethit = true;
                }  
                if (Heli != null)
                {
                    //Baixa la vida del player
                    Heli.hp -= daño;
                }

                if (droide != null)
                {
                    if (hit.transform.tag == "head")
                    {
                        droide.headshoot = true;
                    }
                    else if (hit.transform.tag != "head")
                    {
                        droide.hp -= daño;
                    }
                }
                
                disparado = true;
            }
          
            //Stop la coroutina
            disparado = false;
            transform.transform.GetChild(0).GetChild(2).transform.GetChild(0).gameObject.SetActive(false);

            StopAllCoroutines();

        }
    }

}
