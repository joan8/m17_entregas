﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class barrilP : MonoBehaviour
{

    public bool activado;
    private float radi = 2.0f;
    private float power = 200;
    private float upforce = 1;

    // Start is called before the first frame update
    void Start()
    {
        activado = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (activado)
        {
            this.GetComponent<SphereCollider>().enabled = true; ;
            this.transform.GetChild(0).gameObject.SetActive(false);
            this.transform.GetChild(2).gameObject.SetActive(true);
            Vector3 explosion = this.transform.position;
            Collider[] colliders = Physics.OverlapSphere(explosion,radi);
            
            foreach (Collider hit in colliders)
            {
                
                    print(hit.tag);
                
                Rigidbody rb = hit.GetComponent<Rigidbody>();
                if (rb != null && hit.tag == "droide")
                {
                    rb.AddExplosionForce(power, explosion, radi, upforce, ForceMode.Impulse);
                }
            }
            StartCoroutine(killeffect());
        }
    }
    IEnumerator killeffect()
    {
        yield return new WaitForSeconds(10);
        Destroy(this.gameObject);
    }
}
