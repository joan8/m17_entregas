﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class impactoScript : MonoBehaviour
{
    private bool destroyed;
    public AudioClip audios { set; get; }
    // Start is called before the first frame update
    void Start()
    {
        this.transform.gameObject.GetComponent<AudioSource>().clip = audios;
        this.transform.gameObject.GetComponent<AudioSource>().enabled = false;
        this.transform.gameObject.GetComponent<AudioSource>().enabled = true;
        destroyed = false;
    }

    // Update is called once per frame
    void Update()
    {
 
        StartCoroutine(delete());
    }
    IEnumerator delete()
    {
        yield return new WaitForSeconds(0.04f);
        if (!destroyed) {
            Destroy(this.transform.GetChild(0).gameObject);
            destroyed = true;
        }
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }
}
