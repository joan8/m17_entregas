﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Life : MonoBehaviour
{
    #region Text
    [Header("Texto")]
    public Text txtlife;
    #endregion

    #region Variables
    public int hp { set; get; }
    private int oldhp;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        hp = 100;
        oldhp = hp;
        txtlife.text = hp.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        //Mirar si la vida ha cambiado
        if (oldhp != hp)
        {
            txtlife.text = hp.ToString();
            oldhp = hp;
        }

        if (hp <= 0)
        {
            //End game
            Destroy(this.gameObject);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "barril")
        {
            hp -= 70;
        }
    }

    #region Metodos
    //Evento para curar vida con el botiquin del helicoptero
    public void restoreHP()
    {
        if (hp <= 60)
        {
            hp += 40;
        }
        else if (hp > 60 && hp < 100)
        {
            hp = 100;
        }
    }
    #endregion
}
