﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class droideShoot : MonoBehaviour
{
    #region Cameras
    [Header("Camaras")]
    public Camera cam;
    #endregion

    #region GameObjects
    [Header("GameObjects")]
    public GameObject impacto;
    #endregion

    #region Variables
    private bool disparado;
    private int rango;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        rango = 35;
    }

    // Update is called once per frame
    void Update()
    {
        disparado = false;
        StartCoroutine(Shot());
    }
    #region Coorutinas
    IEnumerator Shot()
    {
        yield return new WaitForSeconds(0.3f);

        RaycastHit hit;


        int layerMask = 1 << 9;
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, rango, layerMask))
        {
            //Agafar el Vector 3 de la camera
            Vector3 forward = cam.transform.TransformDirection(Vector3.forward) * 100000000;
            //Dibuixar el raig del RayCast
            Debug.DrawRay(transform.position, forward, Color.red);
            //Instancia el dispar
            if (!disparado)
            {
                //Agafa que el target es el player
                Life player = hit.transform.GetComponent<Life>();

                GameObject shoot = Instantiate(impacto, hit.point, Quaternion.LookRotation(hit.normal));
                shoot.transform.LookAt(hit.point);
      
                if (player != null)
                {
                    player.hp -= 10;
                }
                disparado = true;
            }
        }
        StopAllCoroutines();
    }
    #endregion
}

