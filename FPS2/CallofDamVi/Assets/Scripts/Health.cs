﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    #region GameEvent
    //Evento para curar vida al personaje y desaparezca al contacto. (Como yo con las chicas vaya)
    public GameEvent restoreHP;
    #endregion

    #region GameObjects
    GameObject jugador;

    #endregion

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        jugador = collision.gameObject;
        if (collision.transform.tag == "player")
        {
            //Evento (Si el jugador tiene la vida al maximo, no saltará el evento)
            if (jugador.GetComponent<Life>().hp < 100)
            {
                //Llama al evento
                restoreHP.Raise();
                Destroy(this.gameObject);
            }

        }
    }
}
