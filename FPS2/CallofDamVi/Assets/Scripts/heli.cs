﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class heli : MonoBehaviour
{
    #region GameObjects
    [Header("GameObjects")]
    public GameObject ammo;
    public GameObject health;
    #endregion

    #region Variables
    private int rotate_speed = 2000;
    public int hp { get; set; }
    private int maxAm;
    private int maxHe;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        hp = 1500;
        maxAm = 2;
        maxHe = 2;
    }

    // Update is called once per frame
    void Update()
    {
        if (0 < hp)
        {
            rotacion();
        }
        else
        {
            this.GetComponent<Rigidbody>().useGravity = true;
        }

        subministros();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "city")
        {
            this.transform.GetChild(16).gameObject.SetActive(true);
            StartCoroutine(final(10));
        }
    }

    #region Metodos
    public void rotacion()
    {
        //Rotacion de las helices
        this.transform.GetChild(5).transform.RotateAround(this.transform.GetChild(5).transform.position, transform.up, Time.deltaTime * -rotate_speed);
        this.transform.GetChild(8).transform.RotateAround(this.transform.GetChild(8).transform.position, transform.right, Time.deltaTime * -rotate_speed);
    }
    public void subministros()
    {
        if (Input.GetKeyDown("p") && 0 < maxAm)
        {
            GameObject AM = Instantiate(ammo);
            AM.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            maxAm--;
        }
        else if (Input.GetKeyDown("o") && 0 < maxHe)
        {
            GameObject AM = Instantiate(health);
            AM.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            maxHe--;
        }
    }
    #endregion

    #region Coorutinas
    IEnumerator final(float time)
    {
        yield return new WaitForSeconds(time);
        this.transform.GetChild(16).gameObject.SetActive(false);
    }
    #endregion

}
