﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class film : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(mute());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator mute()
    {
        yield return new WaitForSecondsRealtime(47);
        this.GetComponent<VideoPlayer>().SetDirectAudioMute(0,true);
    }
}
