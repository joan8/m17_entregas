﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XBullet : MonoBehaviour
{
    private bool colision = false;
    // Start is called before the first frame update
    void Start()
    {
        this.tag = "Shoot";
    }

    // Update is called once per frame
    void Update()
    {

        if (colision)
        {
            this.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            StartCoroutine(destucction());
        }
        else
        {
            StartCoroutine(destucction());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PayerTag" || other.gameObject.tag == "Destructor")
        {
            this.transform.GetChild(0).gameObject.SetActive(false);
            this.transform.GetChild(1).gameObject.SetActive(true);
            colision = true;
        }

    }
    IEnumerator destucction()
    {
        yield return new WaitForSeconds(0.01f);
        this.tag = "Empty";
        yield return new WaitForSeconds(4);
        Destroy(this.gameObject);
    }

}
