﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "NewTime", menuName = "Time Data", order = 1)]
public class scriptable : ScriptableObject
{
    [SerializeField]
    private float bestTime;

    public float BestTime { get; set; }    
}