﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Falcon : MonoBehaviour
{
    public int hp { set; get; }
    private Player player;

    float speed = 600f;

    public Transform target;
    private bool destroyed = false;

    [SerializeField]
    public scriptable time;

    public delegate void M(bool morts);
    public event M eventMort2;

    // Start is called before the first frame update
    void Start()
    {
        hp = 1000;
        //Mira la seguent posicio a la que ha de anar
        this.transform.LookAt(new Vector3(target.position.x, target.position.y, target.position.z));
    }

    // Update is called once per frame
    void Update()
    {
        //Velocitat de l'objecte
        this.transform.Translate(new Vector3(0,0,speed*Time.deltaTime));
        print("Halcon: "+hp);
    }
    public void FixedUpdate()
    {
        if (hp <= 0 && !destroyed)
        {
            Final();
        }
    }
    public void Final()
    {

        if (eventMort2 != null)
        {
            eventMort2(false);
        }
        this.transform.GetChild(1).gameObject.SetActive(true);
        this.transform.GetChild(2).gameObject.SetActive(true);
        this.transform.GetChild(3).gameObject.SetActive(true);
        this.transform.GetChild(4).gameObject.SetActive(true);
        Destroy(this.transform.GetChild(0).gameObject);
        destroyed = true;
        StartCoroutine(final());
        StartCoroutine(destruction());
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "WP")
        {
            this.target = other.gameObject.GetComponent<WayPoint>().nextPoint;
            //Mira el seguent objectiu
            this.transform.LookAt(new Vector3(target.position.x, target.position.y, target.position.z));
        }
    }
    IEnumerator destruction()
    {
        yield return new WaitForSeconds(10);
        this.transform.GetChild(0).gameObject.SetActive(false);
        this.transform.GetChild(1).gameObject.SetActive(false);
        this.transform.GetChild(2).gameObject.SetActive(false);
        this.transform.GetChild(3).gameObject.SetActive(true);
        hp = 1;
    }
    IEnumerator final()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("Win");

    }
}
