﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorstT : MonoBehaviour
{
    [SerializeField]
    private badTime timew;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Text>().text = "Last Worst LifeTime: " + timew.WorstTime;
    }

    // Update is called once per frame
    void Update()
    {
    }
}
