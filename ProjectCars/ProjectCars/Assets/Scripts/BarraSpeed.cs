﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraSpeed : MonoBehaviour
{
    public GameObject player;
    public Transform bar1;

    public float SPEED { get; set; }

    private float SPEEDN;

    // Start is called before the first frame update
    void OnAwake()
    {
        bar1 = transform.Find("Bar1");
        this.transform.localScale = bar1.transform.localScale;
    }
    void Start()
    {

        SPEEDN = SPEED;
        player.GetComponent<Player>().eventSpeed += speed;

    }

    // Update is called once per frame
    void Update()
    {
    }
    public void speed(float speed)
    {
        SetSize(speed);

    }
    public void SetSize(float sizeNormalized)
    {
        SPEEDN = sizeNormalized;

        if (SPEEDN < 0)
        {
            SPEEDN = 0;
        }
        bar1.localScale = new Vector3(SPEEDN, 1.02f);
        this.transform.localScale = new Vector3(3.5f,1.5f);
    }
}
