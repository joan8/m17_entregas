﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewTime", menuName = "TimeBad Data", order = 1)]
public class badTime : ScriptableObject
{
    [SerializeField]
    private float worstTime;


    // Se hace accesor para poder  cogerlo y guardarlo.
    public float WorstTime { get; set; }
}
