﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public float SPEED { set; get; }
    private float ROTATE_SPEED = 50f;
    private float LOOP_SPEED = 200f;
    private float frenada = 2;
    private float disVel = 2;
    private bool colision = false;
    public Texture2D AIM;
    private CursorMode cursorMode = CursorMode.Auto;
    private Vector2 hotSpot = Vector2.zero;
    public Camera Main;
    public Camera Back;
    private bool destroyed = false;

    public float hp { set; get; }

    public delegate void Sp(float speedt);
    public event Sp eventSpeed;

    public delegate void M(bool morts);
    public event M eventMort;

    // Start is called before the first frame update
    void Start()
    {
        //Canviar la imatge del cursor
        Cursor.SetCursor(AIM, hotSpot, cursorMode);
        SPEED = 600f;
        hp = 1000;
        //Posar les cameres que toquen
        Main.enabled = true;
        Back.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        print("Player: " + hp);

        if (hp <= 0 && !destroyed)
        {
            Final();
        }
        //Canvi de cameres
        if (Input.GetKey("f"))
        {
            Main.enabled = false;
            Back.enabled = true;
        }
        else
        {
            Main.enabled = true;
            Back.enabled = false;
        }
    }
    public void FixedUpdate()
    {
        if (!colision)
        {
            //Velocitat
            this.GetComponent<Rigidbody>().velocity = transform.forward * (SPEED / frenada);
    
            ProcessInput();
        }
        else
        {
            StartCoroutine(destruction());

        }
    }

    private void ProcessInput()
    {
        float x = Input.mousePosition.x;
        float y = Input.mousePosition.y;

        //Que es mogui segons la posicio del ratoli

        if (x > (Screen.width / 2 + Screen.width * 0.25) && x < (Screen.width / 2 + Screen.width * 0.75))
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * ROTATE_SPEED);
        }
        else if (x < (Screen.width / 2 - Screen.width * 0.25) && x > (Screen.width / 2 - Screen.width * 0.75))
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * -ROTATE_SPEED);
        }
        else if (x > (Screen.width / 2 + Screen.width * 0.5))
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * (ROTATE_SPEED * 2));
        }
        else if (x < (Screen.width / 2 - Screen.width * 0.5))
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * (-ROTATE_SPEED * 2));
        }

        if (y > (Screen.height / 2 + Screen.height * 0.25))
        {
            transform.RotateAround(transform.position, transform.right, Time.deltaTime * -ROTATE_SPEED);
        }
        else if (y < (Screen.height / 2 - Screen.height * 0.25))
        {
            transform.RotateAround(transform.position, transform.right, Time.deltaTime * ROTATE_SPEED);
        }

        if (Input.GetKey("w"))
        {
            //Accelerar, normalitzar les unitats a la barra de velocitat
            if (frenada >= 1)
            {
                frenada -= 0.05f;
                float velbarr = (SPEED / frenada);
                velbarr = (velbarr/631);
                if (eventSpeed != null)
                {
                    eventSpeed(velbarr);
                }
            }
        }
        //Frenar i el mateix que abans
        if (Input.GetKey("s"))
        {
            if (frenada <= 4)
            {
                float velbarr = (SPEED / frenada);
                velbarr = (velbarr / 631);
                frenada += 0.05f;
                if (eventSpeed != null)
                {
                    eventSpeed(velbarr);
                }
            }
        }
        //Rotar la nau
        if (Input.GetKey("d"))
        {
            transform.RotateAround(transform.position, transform.forward, Time.deltaTime * -LOOP_SPEED);
        }
        if (Input.GetKey("a"))
        {
            transform.RotateAround(transform.position, transform.forward, Time.deltaTime * LOOP_SPEED);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        colision = true;
        Final();
    }
    
    public void Final()
    {
        if (eventMort != null)
        {
            eventMort(false);
        }
        //Activar explosions i destruir la nau
        StartCoroutine(finalLose());
        this.transform.GetChild(1).gameObject.SetActive(true);
        this.transform.GetChild(2).gameObject.SetActive(true);
        this.transform.GetChild(3).gameObject.SetActive(true);
        this.transform.GetChild(4).gameObject.SetActive(true);
        Destroy(this.transform.GetChild(0).gameObject);
        destroyed = true;
        this.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        this.GetComponent<Rigidbody>().rotation = Quaternion.identity;
    }

    IEnumerator destruction()
    {
        yield return new WaitForSeconds(10);
        this.transform.GetChild(0).gameObject.SetActive(false);
        this.transform.GetChild(1).gameObject.SetActive(false);
        this.transform.GetChild(2).gameObject.SetActive(false);
        this.transform.GetChild(3).gameObject.SetActive(true);
    }
    IEnumerator finalLose()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("Lose");

    }
}
