﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerShoot : MonoBehaviour
{
    private Ray ray;
    public Camera MainC;
    public GameObject disparo;
    private float frenada = 2;
    private float disVel = 2;
    private float SPEED = 600f;
    Player player;


    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        SPEED = player.SPEED;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("space"))
        {
            StartCoroutine(Shoot());
        }
    }

    IEnumerator Shoot()
    {
        yield return new WaitForSeconds(0.3f);

        //Declara el hit
        RaycastHit hit;
        //Agafar el Vector 3 de la camera
        Vector3 forward = MainC.transform.TransformDirection(Vector3.forward) * 100000000;
        //Dibuixar el raig del RayCast
        Debug.DrawRay(transform.position, forward, Color.green);
        //Li diem que fassi el ray segons el ratoli
        Ray ray = MainC.ScreenPointToRay(Input.mousePosition);

        //1<<9 son que li dispara a tot el que estigui a la layer 9
        if (Physics.Raycast(ray, out hit, 100000000000000, 1 << 9))
        {
            //Agafa el component Falcon del target i se el guarda 
            Falcon target = hit.transform.GetComponent<Falcon>();
            //Instancia el dispar
            GameObject shoot = Instantiate(disparo, hit.point, Quaternion.LookRotation(hit.normal));

            shoot.transform.LookAt(hit.point);

            if (target !=null)
            {
                //Baixa vida del target
                target.hp -= 100;                
            }
        }
        StopAllCoroutines();
    }
   
}

