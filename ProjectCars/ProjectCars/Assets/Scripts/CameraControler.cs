﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControler : MonoBehaviour
{
    public Camera mainCamera;
    public Player tie;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mainCamera.transform.position = new Vector3
        (
            Mathf.Lerp(mainCamera.transform.position.x, (tie.transform.position.x - (tie.transform.forward.x * 10)), Time.deltaTime * 10),
            Mathf.Lerp(mainCamera.transform.position.y, 6, Time.deltaTime * 10),
            Mathf.Lerp(mainCamera.transform.position.z, (tie.transform.position.z - (tie.transform.forward.z * 10)), Time.deltaTime * 10)
        );

    }
}
