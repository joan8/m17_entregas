﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class gametime : MonoBehaviour
{
    private float valor;
    public GameObject player;
    public Falcon halcon;
    private bool Mort;
    private bool Win;
    [SerializeField]
    public scriptable time;
    public badTime btime;
    #region Delegates

    public delegate void T(float temps);
    public event T eventTime;
    #endregion Delegates


    // Start is called before the first frame update
    void Start()
    {
        Mort = true;
        Win = true;
        player.GetComponent<Player>().eventMort += nonull;
        halcon.eventMort2 += nonullWin;

    }

    // Update is called once per frame
    void Update()
    {
        if (Mort && Win)
        {
            //Cronometre pels temps
            valor = (float)Math.Round(Time.realtimeSinceStartup, 2);
            StartCoroutine(tiempo());
        }
        else if (!Mort && Win)
        {
            btime.WorstTime = valor;
        }
        else if (Mort && !Win)
        {
            time.BestTime = valor;
        }
    }
    public void nonull(bool mort)
    {
        Mort = mort;
    }
    public void nonullWin(bool win)
    {
        Win = win;
    }
    IEnumerator tiempo()
    {
        yield return new WaitForSeconds(0.15f);
        this.GetComponent<Text>().text = "Time: " + valor;
        StopAllCoroutines();
    }
}
