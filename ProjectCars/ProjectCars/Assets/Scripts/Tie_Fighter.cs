﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tie_Fighter : MonoBehaviour
{
    private float SPEED = 0.1f;
    private float ROTATE_SPEED = 100f;
    private float LOOP_SPEED =200f;
    private float frenada = 2;
    private float disVel = 2;
    public GameObject disparo;
    private bool colision = false;
    public Texture2D AIM;
    private CursorMode cursorMode = CursorMode.Auto;
    private Vector2 hotSpot = Vector2.zero;
    private Ray ray;

    private float ScreenW;





    //Script antinc que em va donar problemes, no s'utilitza


















    // Start is called before the first frame update
    void Start()
    {
        Cursor.SetCursor(AIM, hotSpot, cursorMode);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void FixedUpdate()
    {
        if (!colision)
        {
            this.GetComponent<Rigidbody>().velocity = transform.forward * (SPEED / frenada);

            if (Input.GetKey("space"))
            {
                StartCoroutine(Shoot());
            }

            ProcessInput();
        }
        else
        {
            StartCoroutine(destruction());

        }
    }

    private void ProcessInput()
    {
        float x = Input.mousePosition.x;
        float y = Input.mousePosition.y;
        
        if(x > (Screen.width/2 + Screen.width * 0.25))
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * ROTATE_SPEED);
        }
        else if (x < (Screen.width / 2 - Screen.width * 0.25))
        {
            transform.RotateAround(transform.position, transform.up, Time.deltaTime * -ROTATE_SPEED);
        }

        if (y > (Screen.height / 2 + Screen.height * 0.25))
        {
            transform.RotateAround(transform.position, transform.right, Time.deltaTime * -ROTATE_SPEED);
        }
        else if (y < (Screen.height / 2 - Screen.height * 0.25))
        {
            transform.RotateAround(transform.position, transform.right, Time.deltaTime * ROTATE_SPEED);
        }

        if (Input.GetKey("w"))
        {
            if (frenada >= 1)
            {
                frenada -= 0.05f;
            }
        }
        if (Input.GetKey("s")) {
            if (frenada <= 4)
            {
                frenada += 0.05f;
            }
        }
        if (Input.GetKey("d"))
        {
            transform.RotateAround(transform.position, transform.forward, Time.deltaTime * -LOOP_SPEED);
        }  
        if (Input.GetKey("a"))
        {
            transform.RotateAround(transform.position, transform.forward, Time.deltaTime * LOOP_SPEED);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        colision = true;
        this.transform.GetChild(1).gameObject.SetActive(true);
        this.transform.GetChild(2).gameObject.SetActive(true);
        this.transform.GetChild(3).gameObject.SetActive(true);
        this.transform.GetChild(4).gameObject.SetActive(true);
        Destroy(this.transform.GetChild(0).gameObject);
        this.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
        this.GetComponent<Rigidbody>().rotation = Quaternion.identity;
    }

    IEnumerator destruction()
    {
        yield return new WaitForSeconds(10);
        this.transform.GetChild(0).gameObject.SetActive(false);
        this.transform.GetChild(1).gameObject.SetActive(false);
        this.transform.GetChild(2).gameObject.SetActive(false);
        this.transform.GetChild(3).gameObject.SetActive(true);
    }

    IEnumerator Shoot()
    {
        yield return new WaitForSeconds(0.3f);

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        int layerMask = 1 << 9;
        layerMask = ~layerMask;
        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity,layerMask))
        {
            print("Found an object - distance: " + hit.distance);

            //GameObject shoot = Instantiate(disparo);
            GameObject shoot = Instantiate(disparo, hit.point, Quaternion.LookRotation(hit.normal));
            //shoot.transform.LookAt(hit.point);
            float distancia = 2.5f;
            shoot.transform.position = this.transform.position + (this.transform.forward * distancia);
            // shoot.transform.forward = this.transform.forward;
            shoot.GetComponent<Rigidbody>().velocity = shoot.transform.forward * ((SPEED / frenada) * disVel);
        }

        StopAllCoroutines();
    }
}
