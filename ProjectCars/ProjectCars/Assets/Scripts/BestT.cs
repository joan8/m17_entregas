﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BestT : MonoBehaviour
{
    [SerializeField]
    private scriptable time;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Text>().text = "Last Win Time: " + time.BestTime;
    }

    // Update is called once per frame
    void Update()
    {
    }

}
