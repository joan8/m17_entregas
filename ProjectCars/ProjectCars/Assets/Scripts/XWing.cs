﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class XWing : MonoBehaviour
{
    int MoveSpeed = 500;
    public Camera XC;
    public GameObject XShoot;
    private float disVel = 2;

    XMove xwing;

    // Start is called before the first frame update
    void Start()
    {
        xwing = GameObject.Find("X-Wing").GetComponent<XMove>();
    }

    // Update is called once per frame
    void Update()
    {
        //Mira el bool de XMove, per saber si esta dins del rang
        if (xwing.targetAdq)
        {
            StartCoroutine(Shoot());
        }
    }
    IEnumerator Shoot()
    {
        yield return new WaitForSeconds(0.3f);
        //Declara el hit
        RaycastHit hit;
        //Declarar les mascares
        int layerMask = 1 << 8;
        //Que dispari cap endavant de l'objecte
        Vector3 forward = this.transform.TransformDirection(Vector3.forward) * 100000000;
        //Mostrar el Raycast
        Debug.DrawRay(transform.position, forward, Color.red);


        if (Physics.Raycast(XC.transform.position, XC.transform.forward, out hit, 100000000000000, layerMask))
        {
            //Agafa que el target es el player
            Player target = hit.transform.GetComponent<Player>();
            //Instancia el dispar
            GameObject shoot = Instantiate(XShoot, hit.point, Quaternion.LookRotation(hit.normal));
            shoot.transform.LookAt(hit.point);
            if (target != null)
            {
                //Baixa la vida del player
                target.hp -= 100;               
            }
        }
        StopAllCoroutines();
    }
}
