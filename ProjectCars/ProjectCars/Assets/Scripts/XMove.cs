﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XMove : MonoBehaviour
{
    Player player;
    float MoveSpeed = 500;
    int MaxDist = 1000;
    int MinDist = 50;
    public bool targetAdq { set; get; }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();
        targetAdq = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Mira cap el player
        transform.LookAt(player.transform);
        /*Si esta a mes distancia que la minimax, s'aproxima a tu, i si estas per abaix de la maxima,
         *que seria el rang, posa el targetAdq a true, que el pasa a un altres Script per saber si ha de disparar.*/
        if (Vector3.Distance(transform.position, player.transform.position) >= MinDist)
        {

            transform.position += transform.forward * MoveSpeed * Time.deltaTime;



            if (Vector3.Distance(transform.position, player.transform.position) <= MaxDist)
            {

                targetAdq = true;
            }
            else
            {
                targetAdq = false;
            }
        }
    }
}
