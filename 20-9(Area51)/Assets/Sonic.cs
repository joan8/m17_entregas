﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sonic : MonoBehaviour
{
    private static int veloci;
    public int vel;
    private float count, countb;
    private int velmax = 12;
    private int salt =350, minsalt =-12;
    private bool isJump, isGround=true, move, isUp;
    public Animator animator;
    public Muelle muelle;
    public Text score;
    public int punt;
    // Start is called before the first frame update
    void Start()
    {
        animator=gameObject.GetComponent<Animator>();
        isJump = false;
        isGround = true;
        veloci=vel;
    }

    // Update is called once per frame

    void Update()
    {
       score.text = "Score: "+this.punt;            

        /***********************Moviment progresiu***********************/

        if (Input.GetKey("d"))
        {
            if (vel>velmax)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(velmax, 0));    
            }else
            {
               this.GetComponent<Rigidbody2D>().AddForce(new Vector2(vel, 0));        
            }            
        }
        else if (Input.GetKey("a"))
        {

            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-vel, 0));
        }
        else
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0));
        }
        if (Input.GetKeyDown("w")&&!isJump)
        {
            jump();
        }
        else if (Input.GetKey("s") && isGround)
        {
         
           this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -salt/2));
            isUp=false;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 0));
        }
        /***Vacila una mica, no te cap funció especial***/
         if (Input.GetKey("v")){
           animator.SetBool("Vacile",true);
         }else
         {
               animator.SetBool("Vacile",false);
         }

        if (!Input.GetKey("s"))
        {
            isUp=true;
        }

       Movement();
       if (isGround)
       {
           animator.SetBool("Jump",false);
       }
       if(!isUp){
         animator.SetBool("Ball",true);
       }else
       {
          animator.SetBool("Ball",false);
       }
        
    }
    /*******Per l'animació de moviment********/
    private void Movement(){

      if (Input.GetKey("d"))
        {
            move=true;
            animator.SetBool("Move",true);
            count=count+0.1f;
            if(count>1){
                animator.SetBool("Run",true);
            }
        }else{
            move=false;
            animator.SetBool("Move",false);
            count=0f;
             animator.SetBool("Run",false);
        }

        if (Input.GetKey("a"))
        {
            move = true;
            animator.SetBool("Moveb", true);
            countb = countb - 0.1f;
            if (countb < 1)
            {
                animator.SetBool("Runb", true);
            }
        }
        else
        {
            move = false;
            animator.SetBool("Moveb", false);
            countb = 0f;
            animator.SetBool("Runb", false);
        }

    }
    /********Per saltar i amb l'animació*********/
    private void jump(){
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, salt));
            isJump=true;
            isGround = false;
            animator.SetBool("Jump",true);

    }
    
    /********************OnColider***********************/
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "suelo" || collision.gameObject.name == "Nubol" || collision.gameObject.name == "Nubol2" || collision.gameObject.tag == "terra")  
        {
            isJump = false;
            isGround = true;           
            salt=350;
            animator.SetBool("Jump", false);

        }
        /********************Muelle***********************/
        if (collision.gameObject.tag == "meulle")
        {
            isGround=true;
            salt=salt+175;
            jump();
        }


        /*************Turbo en el tub**************/
        if (collision.gameObject.tag == "speed")
        {
            vel = veloci * 2;
            
            
        }
        /*************Turbo cancel**************/

        if (collision.gameObject.name == "NoSpeed")
        {
            Debug.Log(veloci);
            vel = veloci;
        }

        /************************Mort*******************************/
        if (collision.gameObject.name == "pincho" || collision.gameObject.tag == "Fall" || collision.gameObject.name == "Krabit1")
        {
            Destroy(this.gameObject);
        } 
    }
        /*****************Destrucció Monedes***********************/

    private void OnTriggerEnter2D(Collider2D collision){
         if (collision.gameObject.tag == "punts")
        {
            punt++;
            GameObject.Destroy(collision.gameObject);
        }
           if (collision.gameObject.name == "Krabit1")
        {
            GameObject.Destroy(collision.gameObject);
        }
    }
}