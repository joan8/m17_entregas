﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Muelle : MonoBehaviour
{
    public bool onTop;
    GameObject bouncer;
    Animator animator;
    public Vector2 velocity;
    public AudioClip muelle;

    // Start is called before the first frame update
    void Start()
    {
        animator=gameObject.GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
       /* animator.SetBool("colisionM",Col);*/
    }
     private void OnCollisionEnter2D(Collision2D collision)
    {
        onTop = true;
        if (onTop) { 
            this.GetComponent<AudioSource>().PlayOneShot(muelle);
            animator.SetBool("colisionM", true);
            bouncer = collision.gameObject;
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        onTop = false;
        animator.SetBool("colisionM", false);

    }

       void OnTriggerEnter2D()
       {
           onTop = true;

    }
    void OnTriggerExit2D()
    {
        if (gameObject.tag == "meulle") {
            onTop = false;
            animator.SetBool("colisionM", false);
        } 
    }
    void Jump()
    {
        bouncer.GetComponent<Rigidbody2D>().velocity = velocity;

    }
}
