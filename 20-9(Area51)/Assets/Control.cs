﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{

    public Sonic player;
    public Camera camera;
    
    public GameObject[] nubol;

    private float punter;

    private float spawn = 20;
    private int Rand;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate(nubol[0]);
    }

    // Update is called once per frame
    void Update()
    {
        /****************Moviment de la camara********************/
        camera.transform.position = new Vector3(
            player.transform.position.x,
            player.transform.position.y,
            camera.transform.position.z);



        /*************Generació procedural dels nubols************/
    if (player != null && this.punter < player.transform.position.x +spawn )
    {
        GameObject newBlock = Instantiate(nubol[Random.Range(1, nubol.Length)]);
        float size = 12;
        newBlock.transform.position = new Vector2(punter+size/2, Random.Range(3, 7));
        punter +=size;
    }
    }
}
