﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{

    private GameObject player2;
    private GameObject player1;
    public Camera camera;
    private float X, Y;
    // Start is called before the first frame update
    void Start()
    {
        player1 = GameObject.Find("Player1");
        player2 = GameObject.Find("Player2");
       
    }

    // Update is called once per frame
    void Update()
    {
        X = (player2.transform.position.x + player1.transform.position.x) / 2;

        Y = (((player1.transform.position.y + player2.transform.position.y) / 2) > (Y + 0.1)) || ((((player1.transform.position.y + player2.transform.position.y) / 2) < Y - 0.1)) ? ((player1.transform.position.y + player2.transform.position.y) / 2) : Y;
  
        camera.transform.position = new Vector3(
          X,
          Y+1.5f,
          camera.transform.position.z);
    }

    public float getCamera()
    {
        return camera.transform.position.y;
    }
}
