﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSuper : MonoBehaviour
{ 
    private interExSuper super;
    // Start is called before the first frame update
    void Start()
    {
        super = GameObject.Find("BolaSuper").GetComponent<interExSuper>();
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector2(super.getX(), super.getY());
        StartCoroutine(Active(0.5f));
    }
    IEnumerator Active(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        this.gameObject.SetActive(false);
    }
}
