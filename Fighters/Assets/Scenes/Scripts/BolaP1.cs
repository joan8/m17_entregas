﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaP1 : MonoBehaviour
{
    public Animation animator;
    private Player1 player;
    public int vel;
    private bool start, stat;
    private float localinitX, localinitY,X,Y;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player1").GetComponent<Player1>();
        start = false;
    }

    // Update is called once per frame
    void Update()
    {
        colliderRebuild();

        if (player.getBola() || start)
        {
            if (!stat)
            {
                start = true;
                localinitX = player.getX() + 0.5f;
                localinitY = player.getY()+0.25f;
                this.transform.position = new Vector2(localinitX, localinitY);
                this.gameObject.SetActive(true);
                this.GetComponent<Rigidbody2D>().gravityScale = 0;
                stat = true;
            }
            if (player.isChanged)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);

            }    
        }
        StartCoroutine(Active(1));
    }

    private void ResetBola()
    {
        start = false;
        stat = false;
        X = this.transform.position.x;
        Y = this.transform.position.y;
        this.transform.position = new Vector2(localinitX, localinitY);
    }

    private void colliderRebuild()
    {
        if (this.gameObject.GetComponent<PolygonCollider2D>().enabled)
        {
            Destroy(this.gameObject.GetComponent<PolygonCollider2D>());
        }

        PolygonCollider2D p = this.gameObject.AddComponent<PolygonCollider2D>();
        p.isTrigger = true;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player2")
        {
            ResetBola();
            player.transform.GetChild(4).transform.GetChild(1).gameObject.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }
    IEnumerator Active(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        ResetBola();
        this.gameObject.SetActive(false);
    }
    IEnumerator Force(float tiempo1, float tiempo2)
    {
        yield return new WaitForSeconds(tiempo1);
    }

    public bool getStat()
    {
        return stat;
    }
    public bool finalBola()
    {
        return start;
    }

    public float getX()
    {
        return X;
    }
    public float getY()
    {
        return Y;
    }
  
}