﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flechibola : MonoBehaviour
{

    public Animator animator;

    private bool start;
    public float angulox { get; set; }
    public float anguloy { get; set; }

    private float forçax, forçay;

    private float[] valors = new float [6];
    // Start is called before the first frame update
    void Start()
    {
        start = false;
        angulox = 0;
        anguloy = 0;
        valors[0] = 500;
        valors[1] = 1000;
        valors[2] = 1500;
        valors[3] = 2000;
        valors[4] = 2500;
        valors[5] = 3000;
        valors[0] = 3500;
        valors[1] = 4000;
        valors[2] = 4500;
        valors[3] = 5000;
        valors[4] = 5500;
        valors[5] = 6000;
    }

    // Update is called once per frame
    void Update()
    {
        if (!start)
        {
            forçax = valors[Random.Range(0, 6)];
            forçay = valors[Random.Range(0, 6)]/3;
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-forçax, forçay));
            StartCoroutine(stop(0.35f));
        }
    }
  
     void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "faketerra")
        {
            animator.SetBool("Hit", true);
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
            this.transform.tag = "explosio";
            StartCoroutine(Destruct(0.2f));
        }

        if (collision.gameObject.name == "Player1")
        {
            animator.SetBool("Hit", true);
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            this.transform.tag = "explosio";
            StartCoroutine(Destruct(0.2f));
        }
    }
    IEnumerator Destruct(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(this.gameObject);

    }
    IEnumerator stop(float time)
    {
        yield return new WaitForSeconds(time);
        start = true;
    }
}
