﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour
{
    private bool finish;
    // Start is called before the first frame update
    void Start()
    {
        finish = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) && SceneManager.GetActiveScene().name == "Intro")
        {
            SceneManager.LoadScene("Start");
        }
        if (Input.GetKeyDown(KeyCode.Return) && SceneManager.GetActiveScene().name == "Start")
        {
            SceneManager.LoadScene("Characters");
        }
        if (finish)
        {
            Debug.Log("Se saldria del juego");
            Application.Quit();
        }
    }
    public void chooseNamek()
    {
        SceneManager.LoadScene("FightNamek");
    }
    public void chooseOther()
    {
        SceneManager.LoadScene("Other");
    }
    public void chooseRockyField()
    {
        SceneManager.LoadScene("Rock");
    }
    public void chooseBardock()
    {
        SceneManager.LoadScene("Stage");
    }
    public void chooseBroly()
    {
    }
    public void choosePlayer()
    {
        SceneManager.LoadScene("Characters");
    }
    public void Exit()
    {
        finish = true;
    }

}
