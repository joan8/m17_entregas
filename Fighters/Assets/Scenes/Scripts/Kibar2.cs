﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kibar2 : MonoBehaviour
{ 
    private Transform bar;

    public GameObject player2;

    public float ki { get; set; }
    private float kin;

    void Awake()
    {
        bar = transform.Find("KiBar2");
        SetSize1(0);
    }

    // Start is called before the first frame update
    void Start()
    {
        ki = 0;
        kin = ki / 100;
        player2.GetComponent<Broly>().eventKi += KiF;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void KiF(float ki)
    {
        SetSize1(ki / 100);
    }

    public void SetSize1(float sizeNormalized)
    {
        kin = sizeNormalized;

        if (kin < 0)
        {
            kin = 0;
        }
        if (kin > 1)
        {
            kin = 1;
        }

        bar.localScale = new Vector3(kin, 1f);

    }
}
