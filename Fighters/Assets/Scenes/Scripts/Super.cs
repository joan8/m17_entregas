﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Super : MonoBehaviour
{
    public Animation animator;
    private Player1 player;
    public int vel;
    private bool start, stat;
    private float localinitX, localinitY, X, Y;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player1").GetComponent<Player1>();
        start = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.GetComponent<PolygonCollider2D>().enabled)
        {
            Destroy(this.gameObject.GetComponent<PolygonCollider2D>());
        }

        PolygonCollider2D p = this.gameObject.AddComponent<PolygonCollider2D>();
        p.isTrigger = true;

        if (player.getSuper() || start)
        {
            if (!stat)
            {
                start = true;
                localinitX = player.getX() + 0.5f;
                localinitY = player.getY() + 1f;
                this.transform.position = new Vector2(localinitX, localinitY);
                this.gameObject.SetActive(true);
                this.GetComponent<Rigidbody2D>().gravityScale = 0;
                stat = true;
            }
            if (player.isChanged)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, vel);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, vel);

            }

            StartCoroutine(Active(0.5f));
        }
    }

    

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player2")
        {
            start = false;
            stat = false;
            X = this.transform.position.x;
            Y = this.transform.position.y;
            this.transform.position = new Vector2(localinitX, localinitY);
            // player.transform.GetChild(4).transform.GetChild(1).gameObject.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }
    IEnumerator Active(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        this.transform.position = new Vector2(localinitX, localinitY);
        start = false;
        this.gameObject.SetActive(false);
    }
    IEnumerator Force(float tiempo1, float tiempo2)
    {
        yield return new WaitForSeconds(tiempo1);
    }
    public bool finalBola()
    {
        return start;
    }
    public bool getStat()
    {
        return stat;
    }
    public float getX()
    {
        return X;
    }
    public float getY()
    {
        return Y;
    }
   
}