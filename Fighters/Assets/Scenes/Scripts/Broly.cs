﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Broly : MonoBehaviour
{
    public Animator animator;
    public GameObject Gravityball;
    private Player1 player1;
    private GameObject player;

    private bool inAtac, inCombo, onMove, inCharge, inSuper, inGuard, inLevel1, bola, finalC1, isground, FIGHT, getinghHit, usebola, isJump, finito, super;
    private bool tempo = false;
    private bool cBreak { get; set; }
    public bool isChanged { get; set; }
    private bool player1hit { get; set; }
    public float Hp { get; set; }
    public float force;
    private float time, time2,combotime;
    private float damage { get; set; }
    private float x { get; set; }
    private float y { get; set; }
    public float ki { get; set; }  
    private float [] valores = new float [5];
    public int vel;
    private int hitTime { get; set; }
    public int status { get; set; }
    public int prova,salto,fx, fy;

    public delegate void coordenadas(float coorX, float coorY);
    public event coordenadas eventCoor;

    public delegate void Ki(float ki);
    public event Ki eventKi;

    public delegate void hp(float hpt);
    public event hp eventHp;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player2");
        player1 = GameObject.Find("Player1").GetComponent<Player1>();
        finalC1 = true;
        isJump = false;
        isground = true;
        bola = false;
        inAtac = false;
        y = this.transform.position.y;
        Hp = 100;
        ki = 0;
        valores[0] = -1;
        valores[1] = -0.5f;
        valores[2] = 0;
        valores[3] = 0.5f;
        valores[4] = 1;
        FIGHT = false;
        finito = true;
        getinghHit = false;
    }

    // Update is called once per frame
    void Update()
    {
        /**Frase del començament**/
        StartCoroutine(StartFight());

        if (FIGHT)
        {
            /**Recontrucio del colider**/
            rebuildColider();
            if (Input.GetKeyUp(KeyCode.KeypadPlus) || hitTime !=0)
            {
                animator.SetBool("Charge", false);
                this.transform.GetChild(0).gameObject.SetActive(false);
                inCharge = false;
            }
            Ground();
            if (!onMove && !getinghHit && !player1.getinSuper())
            {
                Combos();
                Charge();
            }
            if (!inAtac && !getinghHit && !player1.getinSuper())
            {
                Move();
                Jump(isJump);
            }
            changeSites();
            EndGame();
        }
    }
    
   
    public void Ground()
    {
        if (this.transform.position.y < 1|| !player1hit)
        {
            isground = true;
        }
        else
        {
            isground = false;
        }

        if ((isground || !player1hit) && !player1.getinSuper())
        {

            if (Input.GetKey(KeyCode.Keypad0))
            {
                animator.SetBool("IGuard", true);
                inGuard = true;
            }
            else
            {
                animator.SetBool("IGuard", false);
                inGuard = false;
            }
            
            if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.Keypad0) && (!inCharge || !inSuper || !inLevel1))
            {
                animator.SetBool("DownG", true);
                animator.SetBool("IGuard", false);
                animator.SetBool("Down", false);
            }


            if (Input.GetKey(KeyCode.RightArrow))
            {
                if (isChanged)
                {
                    animator.SetBool("WalkL", false);
                    animator.SetBool("WalkB", true);
                }
                else
                {
                    animator.SetBool("WalkB", false);
                    animator.SetBool("WalkL", true);
                }

            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                if (isChanged)
                {
                    animator.SetBool("WalkL", true);
                    animator.SetBool("WalkB", false);
                }
                else
                {
                    animator.SetBool("WalkB", true);
                    animator.SetBool("WalkL", false);
                }
            }
            else
            {
                animator.SetBool("WalkL", false);
                animator.SetBool("WalkB", false);
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                animator.SetBool("Jump", true);
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                if (Input.GetKeyUp(KeyCode.Keypad0))
                {
                    animator.SetBool("DownG", false);
                }
                animator.SetBool("Down", true);
            }
            else
            {
                animator.SetBool("Down", false);
                animator.SetBool("DownG", false);
                animator.SetBool("Jump", false);
            }
        }
    }
    public void Move()
    {
        if (isground && !inCharge && !inGuard && !inSuper && !inLevel1)
        {
            if (Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.DownArrow))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
                onMove = true;
            }
            else if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.DownArrow))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                onMove = true;
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                onMove = false;
            }


            if (Input.GetKey(KeyCode.DownArrow))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
                onMove = true;
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
                onMove = false;
            }
        }
    }
   
    public void Jump(bool isJ)
    {

        if (isJ && Input.GetKeyDown(KeyCode.UpArrow))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 4000));
            StartCoroutine(Salt(1f));
        }
    }
    public void Combos()
    {
        if (isground)
        {
            Bola();
            Combo1();
            Super();
        }

    }
    public void Bola()
    {
        if (!inGuard && !inCharge && !inSuper && !inCombo)
        {
            if (Input.GetKey(KeyCode.KeypadPeriod) && ki > 5)
            {
                inLevel1 = true;
                inAtac = true;
                bola = true;
                finalC1 = false;
                combotime = 0;
                if (!usebola)
                {
                    animator.SetBool("Bola1", true);
                    usebola = true;
                }
                if (eventKi != null)
                {
                    if (!tempo)
                    {
                        /**S'instancia les boles**/
                        GameObject bolilla = Instantiate(Gravityball) as GameObject;
                        bolilla.transform.parent = player.transform;
                        bolilla.transform.position = new Vector3(this.transform.position.x - 1f, this.transform.position.y + 0.5f, 0);
                        ki -= 5;
                        eventKi(ki);
                        if (ki < 0)
                        {
                            ki = 0;
                        }
                        tempo = true;
                        StartCoroutine(StartBolas(0.15f));
                    }

                }
            }

            if (Input.GetKeyUp(KeyCode.KeypadPeriod))
            {
                bola = false;
                animator.SetBool("Bola1", false);
                usebola = false;
                inAtac = false;
                inLevel1 = false;
            }
        }
    }
    
    public void Combo1()
    {
        if (Input.GetKeyDown(KeyCode.KeypadDivide) && !inGuard && !inCharge && !inLevel1 && !inSuper)
        {
            combotime = 0;
            if (combotime == 0)
            {
                finalC1 = false;
                inAtac = true;
                inCombo = true;
                cBreak = false;
                animator.SetBool("Combo1.1", true);
                if (isChanged)
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(fx, 0));
                }
                else
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-fx, 0));
                }
                this.transform.GetChild(2).gameObject.SetActive(true);
                if (eventKi != null)
                {
                    ki += 3;
                    eventKi(ki);
                }
                combotime++;
            }
            StartCoroutine(MinCombo(0.2f, 1));
            StartCoroutine(MaxCombo(1, status));

        }
        else if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            if (status == 1)
            {
                combotime = 0;
                if (combotime == 0)
                {
                    animator.SetBool("Combo1.2", true);
                    if (isChanged)
                    {
                        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(fx, 0));
                    }
                    else
                    {
                        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-fx, 0));

                    }
                    this.transform.GetChild(3).gameObject.SetActive(true);
                    if (eventKi != null)
                    {
                        ki += 3;
                        eventKi(ki);
                    }
                    combotime++;
                }

                StartCoroutine(MinCombo(0.2f, 2));
                StartCoroutine(MaxCombo(1, status));
            }
            else
            {
                cBreak = true;
            }
        }
        else if (Input.GetKeyDown(KeyCode.KeypadMultiply))
        {
            if (status == 2)
            {
                combotime = 0;
                if (combotime == 0)
                {
                    animator.SetBool("Combo1.3", true);
                    if (isChanged)
                    {
                        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(fx, 0));
                    }
                    else
                    {
                        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-fx, 0));

                    }
                    if (eventKi != null)
                    {
                        ki += 3;
                        eventKi(ki);
                    }
                    combotime++;
                }
                StartCoroutine(MinCombo(0.2f, 3));
                StartCoroutine(MaxCombo(1, status));
            }
            else
            {
                cBreak = true;
            }
        }
        if ((finalC1 && inCombo) || cBreak)
        {
            this.transform.GetChild(2).gameObject.SetActive(false);
            this.transform.GetChild(3).gameObject.SetActive(false);
            animator.SetBool("Combo1.1", false);
            animator.SetBool("Combo1.2", false);
            animator.SetBool("Combo1.3", false);
            combotime = 0;
            inCombo = false;
            inAtac = false;

        }
    }
    public void Super()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter) && !inGuard && !inCharge && !inLevel1 && !inCombo && finito)
        {
            finito = false;
            super = true;
        }
        if (super)
        {
            inSuper = true;
            if (eventCoor != null && player1hit)
            {
                eventCoor(this.transform.position.x, this.transform.position.y);
            }
            if (eventKi != null && ki > 60)
            {
                this.transform.GetChild(9).gameObject.SetActive(true);
                super = true;
                this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 2f);
                this.GetComponent<Rigidbody2D>().gravityScale = 0;
                animator.SetBool("Super", true);
                this.transform.GetChild(10).gameObject.SetActive(true);
                StartCoroutine(startSuper(1.2f));
                ki -= 60;
                eventKi(ki);
            }
            StartCoroutine(Super(2.5f));
            StartCoroutine(finalSuper(2.5f));
        }
    }

    public void correcionCoord()
    {
        x = this.transform.position.x;
        this.transform.position = new Vector3(x, y, 0);
    }
    

   
    public void changeSites()
    {
        if (this.transform.position.x > player1.transform.position.x && isChanged)
        {
            this.transform.Rotate(0, 180, 0);
            isChanged = false;

        }
        else if (this.transform.position.x < player1.transform.position.x && !isChanged)
        {
            this.transform.Rotate(0, 180, 0);
            isChanged = true;
        }
    }
    public void setDamage(float mal)
    {
        damage = mal;
        Hp -= damage;
        if (eventHp != null)
        {
            eventHp(Hp);
        }
    }
    public void getHit()
    {
        getinghHit = true;
        animator.SetBool("Bola1", false);
        animator.SetBool("Combo1.1", false);
        animator.SetBool("Combo1.2", false);
        animator.SetBool("Combo1.3", false);
        animator.SetBool("Down", false);
        animator.SetBool("WalkL", false);
        animator.SetBool("Charge", false);
        StartCoroutine(getHitFinal(0.5f));
    }
    

    public void setCoor(float cx, float cy)
    {
        x = 2f + cx;
        y = 2f + cy;

        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
        this.transform.position = new Vector2(x,y);
    }

    public void rebuildColider()
    {
           if (this.gameObject.GetComponent<PolygonCollider2D>().enabled)
           {
               Destroy(this.gameObject.GetComponent<PolygonCollider2D>());
           }


           PolygonCollider2D p = this.gameObject.AddComponent<PolygonCollider2D>();
    }
    
    public void Charge()
    {
        if (Input.GetKey(KeyCode.KeypadPlus) && !inGuard && !inSuper && !inLevel1 && hitTime == 0)
        {
            animator.SetBool("Charge", true);
            inCharge = true;
            this.transform.GetChild(0).gameObject.SetActive(true);
            if (eventKi != null && ki < 100)
            {
                ki += 0.5f;
                eventKi(ki);
            }
        }
    }
    private void EndGame()
    {
        if (this.Hp <= 0)
        {
            animator.SetBool("Die", true);

        }
        if (player1.Hp <= 0)
        {
            animator.SetBool("Final", true);
            this.transform.GetChild(1).gameObject.SetActive(true);
            StartCoroutine(Final(2f));
        }
    }
    public bool getBola()
    {
        return bola;
    }
    public bool getSuper()
    {
        return super;
    }

    public float getX()
    {
        return this.transform.position.x;
    }
    public float getY()
    {
        return this.transform.position.y;
    }
    public void addTime()
    {
        time++;
    }
    public void addTimeCombo()
    {
        combotime++;
    }
    public float getTime()
    {
        return time;
    }
    public void addTime2()
    {
        time2++;
    }
    public float getTime2()
    {
        return time2;
    }
    public void resetTimeJump()
    {
        time = 0;
        time2 = 0;

    }
    IEnumerator StartBolas(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        tempo = false;
    }
    IEnumerator Super(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);

        this.transform.GetChild(9).gameObject.SetActive(false);
        animator.SetBool("Super", false);
    }

    IEnumerator startSuper(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        this.transform.GetChild(8).transform.GetChild(0).gameObject.SetActive(true);
    }

    IEnumerator MaxCombo(float f, int st)
    {
        yield return new WaitForSeconds(f);
        if (status == st)
        {
            status = 0;
        }
        finalC1 = true;

    }
    IEnumerator MinCombo(float f, int st)
    {

        yield return new WaitForSeconds(f);
        if (!cBreak)
        {
            status = st;
        }
        if (status == 1)
        {
            animator.SetBool("Combo1.1", false);
            this.transform.GetChild(2).gameObject.SetActive(false);
        }
        if (status == 2)
        {
            animator.SetBool("Combo1.2", false);
            this.transform.GetChild(3).gameObject.SetActive(false);
        }
        if (status == 3)
        {
            animator.SetBool("Combo1.3", false);
            finalC1 = true;
        }
    }
    IEnumerator Salt(float tiemps)
    {
        yield return new WaitForSeconds(tiemps);
        isJump = false;
        animator.SetBool("Jump", false);
    }
    IEnumerator Damage(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        this.GetComponent<Rigidbody2D>().gravityScale = 3;
        animator.SetBool("Hit", false);
        hitTime = 0;

    }
    IEnumerator getHitFinal(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        getinghHit = false;
    }
    IEnumerator finalSuper(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        this.GetComponent<Rigidbody2D>().gravityScale = 100;
        this.transform.GetChild(10).gameObject.SetActive(false);
        super = false;
        inSuper = false;
        finito = true;
        StartCoroutine(CoorGravitySuper(0.1f));
    }
    IEnumerator CoorGravitySuper(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        this.GetComponent<Rigidbody2D>().gravityScale = 3;

    }
    IEnumerator StartFight()
    {
        yield return new WaitForSeconds(3);
        this.transform.GetChild(6).gameObject.SetActive(true);
        yield return new WaitForSeconds(4);
        this.transform.GetChild(6).gameObject.SetActive(false);
        FIGHT = true;
    }
    IEnumerator Final(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        SceneManager.LoadScene("Final");
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "bola" && !Input.GetKey(KeyCode.Keypad0))
        {
            getHit();
            animator.SetBool("Hit", true);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(50, 0));
            setDamage(5);
            StartCoroutine(Damage(0.2f));
        }
        if (collision.tag == "medHit" && !Input.GetKey(KeyCode.Keypad0))
        {
            getHit();
            animator.SetBool("Hit", true);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(50, 0));
            if (hitTime == 0)
            {
                setDamage(10);
                hitTime++;
            }
            StartCoroutine(Damage(0.3f));
        }
        if (collision.tag == "presuper" && !Input.GetKey(KeyCode.Keypad0))
        {
            if (hitTime == 0)
            {
                setDamage(5);
                hitTime++;
            }
            hitTime = 0;
            this.GetComponent<Rigidbody2D>().gravityScale = 0;
            player1.GetComponent<Player1>().eventCoor += setCoor;
        }
        if (collision.tag == "super" && !Input.GetKey(KeyCode.Keypad0))
        {
            if (hitTime == 0)
            {
                setDamage(30);
                hitTime++;
                this.transform.position = new Vector2(x, y);
                this.GetComponent<Rigidbody2D>().gravityScale = 100;
            }
            StartCoroutine(Damage(0.3f));
        }
        if (collision.tag == "meteor" && !inSuper)
        {
            getHit();
            animator.SetBool("Hit", true);
            if (hitTime == 0)
            {
                setDamage(1);
                hitTime++;
            }
            StartCoroutine(Damage(0.3f));
        }
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Tilemap" && Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                isJump = true;
            }
        }
    }
}
