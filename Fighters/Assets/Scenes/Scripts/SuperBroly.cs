﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperBroly : MonoBehaviour
{
    public Animator animator;
    private Broly player;
    public int vel;
    private float sizex, sizey;
    private bool start, stat;
    private float localinitX, localinitY, X, Y;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player2").GetComponent<Broly>();
        start = false;
        sizex = this.transform.localScale.x;
        sizey = this.transform.localScale.y;
    }

    // Update is called once per frame
    void Update()
    {

        if (player.getSuper() || start)
        {
            if (!stat)
            {
                start = true;
                localinitX = player.getX() - 0.5f;
                localinitY = player.getY();
                this.transform.position = new Vector2(localinitX, localinitY);
                this.gameObject.SetActive(true);
                this.GetComponent<Rigidbody2D>().gravityScale = 10;
                stat = true;
            }
            if (player.isChanged)
            {
                this.GetComponent<Rigidbody2D>().gravityScale = 1;
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(25,0));
            }
            else
            {
                this.GetComponent<Rigidbody2D>().gravityScale = 1;
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-25,0));
            }
        }
    }

  

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player1")
        {
            this.GetComponent<CircleCollider2D>().isTrigger = false;
            animator.SetBool("Hit", true);
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            this.transform.GetChild(0).gameObject.SetActive(true);
            this.transform.localScale = new Vector2(0.8f, 0.8f);
            this.GetComponent<Rigidbody2D>().gravityScale = 0;
            StartCoroutine(finalBola(0.5f));
        }
        if (collision.gameObject.tag == "faketerra")
        {
            animator.SetBool("Hit", true);
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            this.transform.GetChild(0).gameObject.SetActive(true);
            this.transform.localScale = new Vector2(0.8f, 0.8f);
            this.GetComponent<Rigidbody2D>().gravityScale = 0;
            StartCoroutine(finalBola(0.5f));
        }
    }
    public void Active()
    {
        this.transform.position = new Vector2(localinitX, localinitY);
        start = false;
        stat = false;
        this.transform.localScale = new Vector2(sizex, sizey);
        this.GetComponent<CircleCollider2D>().isTrigger = true;
        this.transform.GetChild(0).gameObject.SetActive(false);
        this.gameObject.SetActive(false);
    }
    IEnumerator Force(float tiempo1, float tiempo2)
    {
        yield return new WaitForSeconds(tiempo1);
    } 
    IEnumerator finalBola(float tiempo1)
    {
        yield return new WaitForSeconds(tiempo1);
        Active();
    }
    public bool finalBola()
    {
        return start;
    }
    public bool getStat()
    {
        return stat;
    }
    public float getX()
    {
        return X;
    }
    public float getY()
    {
        return Y;
    }
  
}
