﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Healthbar2 : MonoBehaviour
{
    private Transform bar2;

    public GameObject player2;

    public float Hp1 { get; set; }
    public float Hp2 { get; set; }
    private float Hp1N, Hp2N;

    void Awake()
    {
        bar2 = transform.Find("Bar2");
        SetColor(Color.white);
    }

    // Start is called before the first frame update
    void Start()
    {
        Hp1 = 100;
        Hp2 = 100;
        Hp1N = Hp1 / 100;
        Hp2N = Hp2 / 100;
        player2.GetComponent<Broly>().eventHp += health;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void health(float hp)
    {
        SetSize(hp / 100);
    }
    public void SetSize(float sizeNormalized)
    {
        Hp1N = sizeNormalized;

        if (Hp1N < 0)
        {
            Hp1N = 0;
        }

        bar2.localScale = new Vector3(Hp1N, 1f);
        if (Hp1N == 1)
        {
            SetColor(Color.white);
        }
        else if (0.8 < Hp1N && Hp1N < 1)
        {
            SetColor(Color.blue);
        }
        else if (0.6 < Hp1N && Hp1N <= 0.8)
        {
            SetColor(Color.cyan);

        }
        else if (0.4 < Hp1N && Hp1N <= 0.6)
        {
            SetColor(Color.green);

        }
        else if (0.2 < Hp1N && Hp1N <= 0.4)
        {
            SetColor(Color.yellow);

        }
        else if (0.01 < Hp1N && Hp1N <= 0.2)
        {
            SetColor(Color.red);

        }
        else if (Hp1N <= 0.01)
        {
            SetColor(Color.grey);
        }
    }

    public void SetColor(Color color)
    {
        bar2.Find("BaraSprite").GetComponent<SpriteRenderer>().color = color;
    }

}
