﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimentProva : MonoBehaviour
{
    private bool dreta, esquerra;

    // Start is called before the first frame update
    void Start()
    {
        dreta = false;
        esquerra = false;

        this.GetComponent<Rigidbody2D>().velocity = new Vector2(2, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x < -6)
        {
            dreta = true;
        }
        if (8 < this.transform.position.x)
        {
            esquerra = true;
        }
        if (dreta)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(2, this.GetComponent<Rigidbody2D>().velocity.y);
            dreta = false;
        }
        if (esquerra)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-2, this.GetComponent<Rigidbody2D>().velocity.y);
            esquerra = false;
        }
       
    }
  
}
