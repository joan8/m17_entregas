﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    private interEx bola;
    // Start is called before the first frame update
    void Start()
    {
        bola = GameObject.Find("Bola").GetComponent<interEx>();
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector2(bola.getX(), bola.getY());
        StartCoroutine(Active(0.5f));
    }
    IEnumerator Active(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        this.gameObject.SetActive(false);
    }
}
