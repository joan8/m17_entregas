﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player1 : MonoBehaviour
{
    public Animator animator;
    private Broly player2;

    private bool inAtac, inCombo, onMove, isJump, inCharge, inSuper, inGuard, inLevel1,finalC1,bola,isground,FIGHT,super;
    private bool cBreak { get; set; }
    public bool isChanged { get; set; }
    private bool player2hit { get; set; }
    private float time, time2,combotime,X, Y,damage;
    public float Hp { get; set; }
    public float ki { get; set; }
    public int vel, salto, fx, fy,prova;
    public int status { get; set; }
    private int hitTime { get; set; }

    public delegate void coordenadas(float coorX, float coorY);
    public event coordenadas eventCoor;

    public delegate void hp(float hpt);
    public event hp eventHp;

    public delegate void Ki(float ki);
    public event Ki eventKi;

    // Start is called before the first frame update
    void Start()
    {
        finalC1 = true;
        isJump = false;
        isground = true;
        bola = false;
        inAtac = false;
        Y = this.transform.position.y;
        Hp = 100;
        ki = 0;
        player2 = GameObject.Find("Player2").GetComponent<Broly>();
        FIGHT = false;
    }

    // Update is called once per frame
    void Update()
    {
        /**Frase del començament**/
        StartCoroutine(StartFight(7));

        if (FIGHT)
        {
            /**Recontrucio del colider**/
            rebuildColider();
            if (Input.GetKeyUp("c") || hitTime != 0)
            {
                animator.SetBool("Charge", false);
                this.transform.GetChild(0).gameObject.SetActive(false);
                inCharge = false;
            }
            Ground();
            if (!onMove)
            {
                Combos();
                Charge();
            }
            if (!inAtac)
            {
                Move();
                Jump(isJump);
            }
            changeSites();
            EndGame();
        }
    }


    

    public void Ground()
    {
         if (this.transform.position.y < 0) {
             isground = true;
         }
         else
         {
             isground = false;
         }
 
        if (isground || player2hit)
        {
            if (Input.GetKey("g") && !inCharge && !inSuper && !inLevel1)
            {
                animator.SetBool("IGuard", true);
                inGuard = true;
            }
            else
            {
                animator.SetBool("IGuard", false);
                inGuard = false;
            }
           
            if (Input.GetKey("s") && Input.GetKey("g") && (!inCharge || !inSuper || !inLevel1))
            {
                animator.SetBool("DownG", true);
                animator.SetBool("IGuard", false);
                animator.SetBool("Down", false);
            }

            if (Input.GetKey("d"))
            {
                if (isChanged)
                {
                    animator.SetBool("WalkL", false);
                    animator.SetBool("WalkB", true);
                }
                else
                {
                    animator.SetBool("WalkB", false);
                    animator.SetBool("WalkL", true);
                }
                
            }else if (Input.GetKey("a"))
            {
                if (isChanged)
                {
                    animator.SetBool("WalkL", true);
                    animator.SetBool("WalkB", false);
                }
                else
                {
                    animator.SetBool("WalkB", true);
                    animator.SetBool("WalkL", false);
                }
            }
            else
            {
                animator.SetBool("WalkL", false);
                animator.SetBool("WalkB", false);
            }
            if (Input.GetKeyDown("w"))
            {
                animator.SetBool("Jump", true);
            }
            else if (Input.GetKey("s"))
            {
                if (Input.GetKeyUp("g"))
                {
                    animator.SetBool("DownG", false);
                }
                animator.SetBool("Down", true);
            }
            else
            {
                animator.SetBool("Down", false);
                animator.SetBool("DownG", false);
                animator.SetBool("Jump", false);
            }
        }
    }

    public void Move()
    {
        if (isground && !inCharge && !inGuard && !inSuper && !inLevel1)
        {
            if (Input.GetKey("d") && !Input.GetKey("s"))
            {
                correcionCoord();
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
                onMove = true;
            }
            else if (Input.GetKey("a") && !Input.GetKey("s"))
            {
                correcionCoord();
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                onMove = true;
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                onMove = false;
            }


            if (Input.GetKey("s"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
                onMove = true;
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
                onMove = false;
            }
        }      
    }
  
    public void Jump(bool isJ)
    {
        
        if (isJ && Input.GetKeyDown("w"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 1200));
            StartCoroutine(Salt(1f));
        }
    }
    public void Combos()
    {
        if (isground)
        {
            Bola();
            Combo1();
            Super();
        }
    }
    public void Bola()
    {
        if (!inGuard && !inCharge && !inSuper && !inCombo)
        {
            if (Input.GetKeyDown("b") && ki > 15)
            {
                inAtac = true;
                bola = true;
                finalC1 = false;
                combotime = 0;
                animator.SetBool("Bola1", true);
                this.transform.GetChild(4).transform.GetChild(0).gameObject.SetActive(true);
                if (eventKi != null)
                {
                    ki -= 15;
                    eventKi(ki);
                }
                StartCoroutine(Bola(0.5f));
            }
        }
    }
    public void Combo1()
    {
        if (Input.GetKeyDown("k") && !inGuard && !inCharge && !inLevel1 && !inSuper)
        {
            combotime = 0;
            if (combotime == 0)
            {
                finalC1 = false;
                inAtac = true;
                inCombo = true;
                cBreak = false;
                animator.SetBool("Combo1.1", true);
                if (isChanged)
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-fx, 0));
                }
                else
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(fx, 0));

                }
                this.transform.GetChild(2).gameObject.SetActive(true);
                if (eventKi != null)
                {
                    ki += 3;
                    eventKi(ki);
                }
                combotime++;
            }
            StartCoroutine(MinCombo(0.2f, 1));
            StartCoroutine(MaxCombo(1, status));

        }
        else if (Input.GetKeyDown("h"))
        {
            if (status == 1)
            {
                combotime = 0;
                if (combotime == 0)
                {
                    animator.SetBool("Combo1.2", true);
                    if (isChanged)
                    {
                        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-fx, 0));
                    }
                    else
                    {
                        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(fx, 0));

                    }
                    this.transform.GetChild(3).gameObject.SetActive(true);
                    if (eventKi != null)
                    {
                        ki += 3;
                        eventKi(ki);
                    }
                    combotime++;
                }

                StartCoroutine(MinCombo(0.2f, 2));
                StartCoroutine(MaxCombo(1, status));
            }
            else
            {
                cBreak = true;
            }
        }
        else if (Input.GetKeyDown("j"))
        {
            if (status == 2)
            {
                combotime = 0;
                if (combotime == 0)
                {
                    animator.SetBool("Combo1.3", true);
                    if (isChanged)
                    {
                        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-fx, fy));
                    }
                    else
                    {
                        this.GetComponent<Rigidbody2D>().AddForce(new Vector2(fx, fy));

                    }
                    if (eventKi != null)
                    {
                        ki += 3;
                        eventKi(ki);
                    }
                    combotime++;
                }
                StartCoroutine(MinCombo(0.2f, 3));
                StartCoroutine(MaxCombo(1, status));
            }
            else
            {
                cBreak = true;
            }
        }
        if ((finalC1 && inCombo) || cBreak)
        {
            this.transform.GetChild(2).gameObject.SetActive(false);
            this.transform.GetChild(3).gameObject.SetActive(false);
            animator.SetBool("Combo1.1", false);
            animator.SetBool("Combo1.2", false);
            animator.SetBool("Combo1.3", false);
            combotime = 0;
            inCombo = false;
            inAtac = false;

        }
    }
    public void Super()
    {
        if (Input.GetKeyDown("u") && !inGuard && !inCharge && !inLevel1 && !inCombo)
        {
            super = true;
        }
        if (super)
        {
            inSuper = true;
            if (eventCoor != null && player2hit)
            {
                eventCoor(this.transform.position.x, this.transform.position.y);
            }
            if (eventKi != null && ki > 60)
            {
                this.transform.GetChild(9).gameObject.SetActive(true);
                super = true;
                this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 1f);
                if (isChanged)
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(-2000, 0));
                }
                else
                {
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(2000, 0));

                }
                animator.SetBool("Super", true);
                StartCoroutine(startSuper(1.5f));
                ki -= 60;
                eventKi(ki);
            }
            StartCoroutine(Super(2.5f));
        }
    }

    public void correcionCoord()
    {
        X = this.transform.position.x;
        this.transform.position = new Vector3(X, Y, 0);
    }
   
    public void changeSites()
    {
        if (this.transform.position.x < player2.transform.position.x && isChanged)
        {
            this.transform.Rotate(0, 180, 0);
            isChanged = false;

        }
        else if (this.transform.position.x > player2.transform.position.x && !isChanged)
        {
            this.transform.Rotate(0, 180, 0);
            isChanged = true;
        }
    }
    public void rebuildColider()
    {
        if (this.gameObject.GetComponent<PolygonCollider2D>().enabled)
        {
            Destroy(this.gameObject.GetComponent<PolygonCollider2D>());
        }
        this.gameObject.AddComponent<PolygonCollider2D>();
        this.gameObject.GetComponent<PolygonCollider2D>().isTrigger = true;
    }

    public void setDamage(float mal)
    {
        damage = mal;
        Hp -= damage;
        if (eventHp != null)
        {
            eventHp(Hp);
        }
    }

    public void getHit()
    {
        animator.SetBool("Bola1", false);
        animator.SetBool("Combo1.1", false);
        animator.SetBool("Combo1.2", false);
        animator.SetBool("Combo1.3", false);
        animator.SetBool("Down", false);
        animator.SetBool("WalkL", false);
        animator.SetBool("Charge", false);
    }

    public void setCoor(float cx, float cy)
    {
        X = 2f + cx;
        Y = 2f + cy;

        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        this.transform.position = new Vector2(X, Y);
    }
    public void Charge()
    {
        if (Input.GetKey("c") && !inGuard && !inSuper && !inLevel1 && hitTime == 0)
        {
            animator.SetBool("Charge", true);
            inCharge = true;
            this.transform.GetChild(0).gameObject.SetActive(true);
            if (eventKi != null && ki < 100)
            {
                ki += 0.5f;
                eventKi(ki);
            }
        }
    }
    private void EndGame()
    {
        if (this.Hp <= 0)
        {
            animator.SetBool("Die", true);
        }
        if (player2.Hp <= 0)
        {
            animator.SetBool("Final", true);
            this.transform.GetChild(11).gameObject.SetActive(true);
            StartCoroutine(Final(4f));
        }
    }
    IEnumerator Salt(float tiemps)
    {
        yield return new WaitForSeconds(tiemps);
        isJump = false;
        animator.SetBool("Jump", false);
    }
    IEnumerator Damage(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);

        animator.SetBool("Hit", false);
        hitTime = 0;
    }
    IEnumerator StartFight(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        FIGHT = true;
        this.transform.GetChild(5).gameObject.SetActive(false);
    }
    IEnumerator Final(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        SceneManager.LoadScene("Final");
    }

    IEnumerator Bola(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);

        bola = false;
        animator.SetBool("Bola1", false);
        inAtac = false;
    }
    IEnumerator Super(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        this.transform.GetChild(10).gameObject.SetActive(false);
        this.transform.GetChild(9).gameObject.SetActive(false);
        super = false;
        inSuper = false;
        animator.SetBool("Super", false);
    }

    IEnumerator startSuper(float tiempo)
    {
        yield return new WaitForSeconds(0.8f);
        this.transform.GetChild(10).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.7f);
        this.transform.GetChild(7).gameObject.SetActive(true);
    }

    IEnumerator MaxCombo(float f, int st)
    {
        yield return new WaitForSeconds(f);
        if (status == st)
        {
            status = 0;
        }
        finalC1 = true;
    }
    IEnumerator MinCombo(float f, int st)
    {

        yield return new WaitForSeconds(f);
        if (!cBreak)
        {
            status = st;
        }
        if (status == 1)
        {
            animator.SetBool("Combo1.1", false);
            this.transform.GetChild(2).gameObject.SetActive(false);
        }
        if (status == 2)
        {
            animator.SetBool("Combo1.2", false);
            this.transform.GetChild(3).gameObject.SetActive(false);
        }
        if (status == 3)
        {
            animator.SetBool("Combo1.3", false);
            finalC1 = true;
        }
    }

   
    public bool getinSuper()
    {
        return inSuper;
    }
    public bool getBola()
    {
        return bola;
    }
    public bool getSuper()
    {
        return super;
    }

    public float getX()
    {
        return this.transform.position.x;
    }
    public float getY()
    {
        return this.transform.position.y;
    }
    public void addTime()
    {
        time++;
    }
    public void addTimeCombo()
    {
        combotime++;
    }
    public float getTime()
    {
        return time;
    }
    public void addTime2()
    {
        time2++;
    }
    public float getTime2()
    {
        return time2;
    }
    public void resetTimeJump()
    {
        time = 0;
        time2 = 0;

    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "bola2" && !Input.GetKey("g"))
        {
            getHit();
            animator.SetBool("Hit", true);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(50, 0));
            setDamage(5);
            StartCoroutine(Damage(0.2f));

        }
        if (collision.tag == "medHit2" && !Input.GetKey("g"))
        {
            getHit();
            animator.SetBool("Hit", true);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(50, 0));
            if (hitTime == 0)
            {
                setDamage(10);
                hitTime++;
            }
            StartCoroutine(Damage(0.3f));

        }
        if (collision.tag == "presuper2" && !Input.GetKey("g"))
        {
            if (hitTime == 0)
            {
                setDamage(5);
                hitTime++;
            }
            hitTime = 0;
            this.GetComponent<Rigidbody2D>().gravityScale = 0;
            player2.GetComponent<Broly>().eventCoor += setCoor;
        }
        if (collision.tag == "super2" && !Input.GetKey("g"))
        {
            if (hitTime == 0)
            {
                setDamage(40);
                hitTime++;
            }
            StartCoroutine(Damage(0.3f));
        }

        if (collision.tag == "super2" && Input.GetKey("g"))
        {
            if (hitTime == 0)
            {
                setDamage(5);
                hitTime++;
            }
            StartCoroutine(Damage(0.3f));
        }

        if (collision.tag == "bolilla" && !Input.GetKey("g"))
        {
            getHit();
            animator.SetBool("Hit", true);
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(50, 0));
            if (hitTime == 0)
            {
                setDamage(5);
                hitTime++;
            }
            StartCoroutine(Damage(0.3f));

        }
        if (collision.gameObject.name == "Player2" && super)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
        if (collision.tag == "meteor" && !inSuper)
        {
            getHit();
            animator.SetBool("Hit", true);
            if (hitTime == 0)
            {
                setDamage(1);
                hitTime++;
            }
            StartCoroutine(Damage(0.3f));
        }
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Tilemap" && Input.GetKeyDown("w"))
        {
            if (Input.GetKeyDown("w"))
            {
                isJump = true;
            }

        }
        if (collision.gameObject.name == "Player2")
        {
            player2hit = true;

        }
        else
        {
            player2hit = false;
        }
    }
}
