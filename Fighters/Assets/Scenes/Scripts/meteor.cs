﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meteor : MonoBehaviour
{
    public GameObject Meteor;
    private bool tempo;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {   
            if (!tempo)
            {
                GameObject meteor = Instantiate(Meteor) as GameObject; 
                tempo = true;
                StartCoroutine(StartBolas(0.3f));
            } 
    }
    IEnumerator StartBolas(float tiempo)
    {
        yield return new WaitForSeconds(tiempo);
        tempo = false;
    }
}
